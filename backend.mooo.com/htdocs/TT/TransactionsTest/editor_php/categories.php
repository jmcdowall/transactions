<?php

// DataTables PHP library
include( "../editor/php/DataTables.php" );

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
	
$userid = 1;


	Editor::inst( $db, 'Categories' )
		->field( 
			Field::inst( 'Categories.Name' ),
			Field::inst( 'Categories.CategoryGroup')
			->options('CategoryGroups','id','name',function ($q) {
        $q->where( 'CategoryGroups.user_id', $GLOBALS['userid'] );
    })
			->validator( 'Validate::dbValues' ),
			Field::inst( 'Categories.Type')
			->options('CategoryTypes','id','name')
			->validator( 'Validate::dbValues' ),
			Field::inst( 'Categories.user_id')
			->setValue( $userid ),
			Field::inst( 'CategoryGroups.name'),
			FIeld::inst( 'CategoryTypes.name')
		)
		->where( 'Categories.user_id', $userid )
		->leftJoin('CategoryGroups','CategoryGroups.id','=','Categories.CategoryGroup')
		->leftJoin('CategoryTypes','CategoryTypes.id','=','Categories.Type')
		->process($_POST)
		->json();
