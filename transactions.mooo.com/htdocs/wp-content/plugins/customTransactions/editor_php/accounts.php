<?php

// DataTables PHP library
include( "../editor/php/DataTables.php" );

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
	
$userid = 1;


	Editor::inst( $db, 'Accounts' )
		->field( 
			Field::inst( 'Accounts.Name' ),
			Field::inst( 'Accounts.AccountGroup')
			->options('AccountGroups','id','name',function ($q) {
        $q->where( 'AccountGroups.user_id', $GLOBALS['userid'] );
    })
			->validator( 'Validate::dbValues' ),
			Field::inst( 'Accounts.AccountType')
			->options('AccountTypes','id','name')
			->validator( 'Validate::dbValues' ),
			Field::inst( 'Accounts.user_id')
			->setValue( $userid ),
			Field::inst( 'AccountGroups.name'),
			FIeld::inst( 'AccountTypes.name'),
			Field::inst( 'Accounts.OtherGrouping'),
			Field::inst( 'Accounts.InCash'),
			Field::inst( 'Accounts.InNetWorth')
		)
		->where( 'Accounts.user_id', $userid )
		->leftJoin('AccountGroups','AccountGroups.id','=','Accounts.AccountGroup')
		->leftJoin('AccountTypes','AccountTypes.id','=','Accounts.AccountType')
		->process($_POST)
		->json();
