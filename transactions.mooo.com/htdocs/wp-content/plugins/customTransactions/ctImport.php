<?php

include 'PHPExcel/Classes/PHPExcel.php';


function ct_shortcode_importExcel(){
  
  if(isset($_FILES['excelFileUpload'])){
      $errorstring= "";
      $file_name = $_FILES['excelFileUpload']['name'];
      $file_size =$_FILES['excelFileUpload']['size'];
      $file_tmp = $_FILES['excelFileUpload']['tmp_name'];
      $file_type = $_FILES['excelFileUpload']['type'];
      $file_name_array = explode('.',$file_name);
      $file_ext = strtolower(end($file_name_array));
      
      $extensions = array("xlsx");
      
      if(in_array($file_ext,$extensions)=== false){
        $errorstring .= "Extension not allowed, please choose a XLSX file.<BR>";
      }
      
      if($file_size > 20971520){
         $errorstring .= 'File size must be less than 2 MB<BR>';
      }
      
      if(strlen($errorstring)==0){
        $errorstring = "Success<BR>";
        //Excel File Processing
        $excelResult = ct_processExcel($_FILES['excelFileUpload']['tmp_name']);
        
      }else{
        
        
        $excelResult = "";
        
      }
   }
  else {$errorstring = ''; $excelResult = "";}
  
  
  $return = 'This is the Excel Import Location';
  
  $return .=  '<form action="" method="POST" enctype="multipart/form-data">
         <input type="file" name="excelFileUpload" />
         <input type="submit" Value="Upload"/>
      </form>';
  
  return $errorstring.$return;
  
}

function ct_processExcel($filename){
  
  $return = '';
  $readError = false;
  
  //Create New PHP Excel Object
  $excelFileReader = new PHPExcel_Reader_Excel2007();
  $excelFileReader->setReadDataOnly(true);
  
  //Load The File
  try{
    $excelFile = $excelFileReader->load($filename);
     }
  catch(PHPExcel_Reader_Exception $e) {
    $return .= "Error Loading the File";
    $return .= $e->getMessage();
    $readError = true;
}
  
  //Read the contents of the Excel File
  if(!$readError){
    
    //Get the sheets names and confirm all expected sheets exist
    $sheetnames = $excelFile->getSheetNames();
    
    //Set a array item with each sheet name as the index for easilt determining if each exists.
    foreach($sheetnames as $currentsheetname){
      $sheetnames[$currentsheetname] = true;
    }
    
    $sheetExists_Tags = isset($sheetnames['Tags']);
    $sheetExists_Categories = isset($sheetnames['Categories']);
    $sheetExists_Accounts = isset($sheetnames['Accounts']);
    $sheetExists_Transactions = isset($sheetnames['Transactions']);
    
    //print_r($sheetnames);
    
    //Only Proceed with loading data if all sheets exist
    if($sheetExists_Tags AND $sheetExists_Categories AND $sheetExists_Accounts AND $sheetExists_Transactions){
            
      //Load Each Worksheet Into an Array
      $excelFile->setActiveSheetIndexByName('Tags');    
      $sheetData['Tags'] = $excelFile->getActiveSheet()->toArray(null, true, true, true);
      
      $excelFile->setActiveSheetIndexByName('Accounts');
      $sheetData['Accounts'] = $excelFile->getActiveSheet()->toArray(null, true, true, true);

      $excelFile->setActiveSheetIndexByName('Categories');
      $sheetData['Categories'] = $excelFile->getActiveSheet()->toArray(null, true, true, true);

      $excelFile->setActiveSheetIndexByName('Transactions');
      $sheetData['Transactions'] = $excelFile->getActiveSheet()->toArray(null, true, true, true);
      
      //Build the data array for processing
      ct_import_buildarray($sheetData);
      
      //echo $sheet;
      //print_r($sheetnames);
      //print_r($sheetData);      
    }
     
    
  } 
  return $return;  
}

function ct_import_buildarray($sheetData){

  $importArray = array("Tags" => "","TagGroups" => "","Categories" => "", "CategoryGroups" => "", "Accounts" => "", "AccountGroups" => "", "Transactions" => "");
  
  $checkArray = array(array('Sheet' => 'Tags', 'Row' => '1', 'Column' => 'A', 'Value' => 'Tag Groups'),
                      array('Sheet' => 'Tags', 'Row' => '1', 'Column' => 'C', 'Value' => 'Tag'),
                      array('Sheet' => 'Tags', 'Row' => '1', 'Column' => 'D', 'Value' => 'Tag Group'),
                      array('Sheet' => 'Categories', 'Row' => '1', 'Column' => 'A', 'Value' => 'Category Groups')
                     );
  
  $check = true;
  
  //Checks that each of the check locations match what is expected.
  foreach($checkArray as $currentCheck){
    if($sheetData[$currentCheck['Sheet']][$currentCheck['Row']][$currentCheck['Column']] == $currentCheck['Value']){ }
    else{$check = false;}
  }
  
  //If not all of the check information is correct, then make an error, otherwise process the array.
  if($check){
    
    //Build Tag and Tag Group Array
    ct_import_tagInfo($sheetData['Tags'],$errorstring, $importArray['Tags'],$importArray['TagGroups'] );
    
    //Build Category and Category Group Array
    ct_import_categoryInfo($sheetData['Categories'], $errorstring, $importArray['Categories'],$importArray['CategoryGroups']);
    
		//Build Account and Account Group Array
    ct_import_accountInfo($sheetData['Accounts'], $errorstring, $importArray['Accounts'],$importArray['AccountGroups']);
    
		//Build Transaction Array
    ct_import_transactionInfo($sheetData['Transactions'], $errorstring, $importArray['Transactions'], $importArray['Tags'],  $importArray['Categories'],$importArray['Accounts']);
    
		
    //print_r($importArray);
    ct_import_TableDisplay($importArray);
  }
  else
  {}
  
}

//Displays the Imported Data in a table
function ct_import_TableDisplay($importArray){

  $tableString = '<table id="importData_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Type</th>
						<th>Data</th>
						<th>Error</th>
						<th>Message</th>
					</tr>

				</thead>';
  
  foreach($importArray as $currentTypeName => $currentTypeData){
    foreach($currentTypeData as $item){
      
      //New Table Row
      $tableString .= '<tr>';
      
      //Build Information To GO Into Data Column
      switch ($currentTypeName){
        case "Tags" : 
          $typeString = "Tag";
          if(strlen($item['Group']) > 0){ $dataForDisplay = $item['Group'].":".$item['Name'];}
          else{$dataForDisplay = $item['Name'];}
          break;
          
         case "TagGroups" : 
          $typeString = "Tag Group";
          $dataForDisplay = $item['Name'];
          break;
          
         case "Categories" : 
          $typeString = "Category";
          if(strlen($item['Group']) > 0){ $dataForDisplay = $item['Group'].":".$item['Name'];}
          else{$dataForDisplay = $item['Name'];}
          if(strlen($item['Description']) > 0){ $dataForDisplay .= "<BR>Description=".$item['Description'];}
          if(strlen($item['Type']) > 0){ $dataForDisplay .= "<BR>Type=".$item['Type'];}
          if(strlen($item['OtherGrouping']) > 0){ $dataForDisplay .= "<BR>Other Grouping=".$item['OtherGrouping'];}
          break;
          
         case "CategoryGroups" : 
          $typeString = "Category Group";
          $dataForDisplay = $item['Name'];
          break;
          
          case "Accounts" : 
          $typeString = "Account";
            if(strlen($item['Group']) > 0){ $dataForDisplay = $item['Group'].":".$item['Name'];}
          else{$dataForDisplay = $item['Name'];}
          if(strlen($item['Description']) > 0){ $dataForDisplay .= "<BR>Description=".$item['Description'];}
          if(strlen($item['Type']) > 0){ $dataForDisplay .= "<BR>Type=".$item['Type'];}
          if(strlen($item['OtherGrouping']) > 0){ $dataForDisplay .= "<BR>Other Grouping=".$item['OtherGrouping'];}
					$dataForDisplay .= "<BR>In Net Worth = ".$item['InNetWorth'];
					$dataForDisplay .= "<BR>In Cash = ".$item['InCash'];
          break;
          
         case "AccountGroups" : 
          $typeString = "Account Group";
          $dataForDisplay = $item['Name'];
          break;
        }
      
      //Build Row Table Data
      $tableString .= "<td>$typeString</td>";
      $tableString .= "<td>$dataForDisplay</td>";
      if($item['Error'] < 1){
        $tableString .= "<td></td>";
      }
      else{
        $tableString .= "<td>Error</td>";
      }
      $tableString .= "<td>".$item['Message']."</td>";
      
      
      //End Table Row
      $tableString .= '</tr>';
    }
  }
  
  $tableString .='</table>';
    
  echo $tableString;
  
}

//Builds the Account and Account Group array from the data array from the excel file.
function ct_import_accountInfo($data,&$errorstring,&$accountArray, &$accountGroupArray)
{
  //Account Group Information Location
  $groupNameRow = 'A';
  
  //Account Information Location
  $accountNameRow = 'C';
  $accountDescriptionRow = 'D';
  $accountGroupRow = 'E';
  $accountTypeRow = 'F';
  $accountOtherGroupingRow = 'G';
	$accountInCashRow = 'H';
	$accountInNetWorthRow = 'I';
  
  $accountGroupArray = array();
  $accountArray = array();
  
  $i = 0;

  $firstRow = true;
  
  //Build Account Group List
  foreach($data as $currentRow){
    
    //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
        if(strlen($currentRow[$groupNameRow]) > 0){
          
          $duplicate = false;
          //Check if Account Group is a duplicate.
          foreach($accountGroupArray as $currentAccountGroup){
            if(strtolower($currentAccountGroup['Name']) == strtolower($currentRow[$groupNameRow])){
              $duplicate = true;
            }
          }
          
          $accountGroupArray[$i]['Name'] =  $currentRow[$groupNameRow];

          

          if($duplicate == true){
            $accountGroupArray[$i]['Error'] = 1;
            $accountGroupArray[$i]['Message'] = "Duplicate Account Group Name";
          }
          else {
            $accountGroupArray[$i]['Error'] = 0;
            $accountGroupArray[$i]['Message'] = "";
          }

          $i = $i + 1;
        }
    }
  }
  
  $i = 0;
  $firstRow = true;
  
  
  //Build Account List
  foreach($data as $currentRow){
    
     //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
      if(isset($currentRow[$accountNameRow])){
        if(strlen($currentRow[$accountNameRow]) > 0){   
          
          //Check for duplicate tags
          $duplicate = false;

          foreach($accountArray as $currentAccount){
            if($currentAccount['Name'] == $currentRow[$accountNameRow] && $currentAccount['Group'] == $currentRow[$accountGroupRow]){
              $duplicate = true;
            }
          }
          
          $accountArray[$i]['Name'] =  $currentRow[$accountNameRow];
          $accountArray[$i]['Group'] =  $currentRow[$accountGroupRow];
          $accountArray[$i]['Error'] = 0;
          $accountArray[$i]['Message'] = "";
          $accountArray[$i]['Description'] = $currentRow[$accountDescriptionRow];
          $accountArray[$i]['Type'] = $currentRow[$accountTypeRow];
          $accountArray[$i]['OtherGrouping'] = $currentRow[$accountOtherGroupingRow];
					$accountArray[$i]['InNetWorth'] = $currentRow[$accountInNetWorthRow];
					$accountArray[$i]['InCash'] = $currentRow[$accountInCashRow];

					//Check if Account is a duplicate
          if($duplicate){
            $accountArray[$i]['Error'] = 1;
            $accountArray[$i]['Message'] .= "Duplicate Account";
          }
					
					//Check that In Cash Is Valid
					if($accountArray[$i]['InCash'] == "Yes" || $accountArray[$i]['InCash'] == "No")
					{}
					else{$accountArray[$i]['Error'] = 1;
						if(strlen( $accountArray[$i]['Message']) > 0){$accountArray[$i]['Message'] .= "<BR>";}
            $accountArray[$i]['Message'] .= "In Cash Column Set Incorrectly";}
					
					//Check that In Net Worth Is Valid
					if($accountArray[$i]['InNetWorth'] == "Yes" || $accountArray[$i]['InNetWorth'] == "No")
					{}
					else{$accountArray[$i]['Error'] = 1;
						if(strlen( $accountArray[$i]['Message']) > 0){$accountArray[$i]['Message'] .= "<BR>";}
            $accountArray[$i]['Message'] .= "In Net Worth Column Set Incorrectly";}
					
					//Check Account Type Is Valid
					if($accountArray[$i]['Type'] == "Asset" 
						|| $accountArray[$i]['Type'] == "Cash"
						|| $accountArray[$i]['Type'] == "Liability"
						|| $accountArray[$i]['Type'] == "Investment"
						|| $accountArray[$i]['Type'] == "Retirement"
						|| $accountArray[$i]['Type'] == "Savings"
						|| $accountArray[$i]['Type'] == "Term Deposit")
					{}
					else{$accountArray[$i]['Error'] = 1;
						if(strlen( $accountArray[$i]['Message']) > 0){$accountArray[$i]['Message'] .= "<BR>";}
            $accountArray[$i]['Message'] .= "Account Type Column Set Incorrectly";}

          ///Check That Account Group Exists In Account Group List
          $exists = false;

          if(strlen($accountArray[$i]['Group']) > 0){
            foreach($accountGroupArray as $currentAccountGroup){
              if(strtolower($currentAccountGroup['Name']) == strtolower($accountArray[$i]['Group'])){
                $exists = true;
              } 
            }          
          }
          else
          {
            $exists = true;
          }

          if(!$exists){
            $accountArray[$i]['Error'] = 1;
            $accountArray[$i]['Message'] .= "Account Group ".$accountArray[$i]['Group']." Doesn't Exist";
          }

          $i = $i + 1;
        } 
    }
    }
  }
}

//Builds the Category and Category Group array from the data array from the excel file.
function ct_import_categoryInfo($data,&$errorstring,&$categoryArray, &$categoryGroupArray)
{
  //Category Group Information Location
  $groupNameRow = 'A';
  
  //Tag Information Location
  $categoryNameRow = 'C';
  $categoryDescriptionRow = 'D';
  $categoryGroupRow = 'E';
  $categoryTypeRow = 'F';
  $categoryOtherGroupingRow = 'G';
  
  $categoryGroupArray = array();
  $categoryArray = array();
  
  $i = 0;

  $firstRow = true;
  
  //Build Category Group List
  foreach($data as $currentRow){
    
    //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
        if(strlen($currentRow[$groupNameRow]) > 0){
          
          $duplicate = false;
          //Check if Category Group is a duplicate.
          foreach($categoryGroupArray as $currentCategoryGroup){
            if(strtolower($currentCategoryGroup['Name']) == strtolower($currentRow[$groupNameRow])){
              $duplicate = true;
            }
          }
          
          $categoryGroupArray[$i]['Name'] =  $currentRow[$groupNameRow];

          

          if($duplicate == true){
            $categoryGroupArray[$i]['Error'] = 1;
            $categoryGroupArray[$i]['Message'] = "Duplicate Category Group Name";
          }
          else {
            $categoryGroupArray[$i]['Error'] = 0;
            $categoryGroupArray[$i]['Message'] = "";
          }

          $i = $i + 1;
        }
    }
  }
  
  $i = 0;
  $firstRow = true;
  
  
  //Build Category List
  foreach($data as $currentRow){
    
     //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
      if(isset($currentRow[$categoryNameRow])){
        if(strlen($currentRow[$categoryNameRow]) > 0){   
          
          //Check for duplicate tags
          $duplicate = false;

          foreach($categoryArray as $currentCategory){
            if($currentCategory['Name'] == $currentRow[$categoryNameRow] && $currentCategory['Group'] == $currentRow[$categoryGroupRow]){
              $duplicate = true;
            }
          }
          
          $categoryArray[$i]['Name'] =  $currentRow[$categoryNameRow];
          $categoryArray[$i]['Group'] =  $currentRow[$categoryGroupRow];
          $categoryArray[$i]['Error'] = 0;
          $categoryArray[$i]['Message'] = "";
          $categoryArray[$i]['Description'] = $currentRow[$categoryDescriptionRow];
          $categoryArray[$i]['Type'] = $currentRow[$categoryTypeRow];
          $categoryArray[$i]['OtherGrouping'] = $currentRow[$categoryOtherGroupingRow];

          

          if($duplicate){
            $categoryArray[$i]['Error'] = 1;
            $categoryArray[$i]['Message'] .= "Duplicate Category";
          }

          ///Check That Category Group Exists In Category Group List
          $exists = false;

          if(strlen($categoryArray[$i]['Group']) > 0){
            foreach($categoryGroupArray as $currentCategoryGroup){
              if(strtolower($currentCategoryGroup['Name']) == strtolower($categoryArray[$i]['Group'])){
                $exists = true;
              } 
            }          
          }
          else
          {
            $exists = true;
          }

          if(!$exists){
            $categoryArray[$i]['Error'] = 1;
            $categoryArray[$i]['Message'] .= "Category Group ".$categoryArray[$i]['Group']." Doesn't Exist";
          }


          $i = $i + 1;
        } 
    }
    }
  }
}

//Builds the Tag and Tag Group array from the data array from the excel file.
function ct_import_tagInfo($data,&$errorstring,&$tagArray, &$tagGroupArray)
{
  //Tag Group Information Location
  $groupNameRow = 'A';
  
  //Tag Information Location
  $tagNameRow = 'C';
  $tagGroupRow = 'D';
  
  $tagGroupArray = array();
  $tagArray = array();
  
  $i = 0;

  $firstRow = true;
  
  //Build Tag Group List
  foreach($data as $currentRow){
    
    //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
      //if(isset($currentRow[$groupNameRow])){
        if(strlen($currentRow[$groupNameRow]) > 0){
          
          $duplicate = false;
          //Check if Tag Group is a duplicate.
          foreach($tagGroupArray as $currentTagGroup){
            if(strtolower($currentTagGroup['Name']) == strtolower($currentRow[$groupNameRow])){
              $duplicate = true;
            }
          }
          
          $tagGroupArray[$i]['Name'] =  $currentRow[$groupNameRow];

          

          if($duplicate == true){
            $tagGroupArray[$i]['Error'] = 1;
            $tagGroupArray[$i]['Message'] = "Duplicate Tag Group Name";
          }
          else {
            $tagGroupArray[$i]['Error'] = 0;
            $tagGroupArray[$i]['Message'] = "";
          }

          $i = $i + 1;
        }
    }
  }
  
  $i = 0;
  $firstRow = true;
  
  
  //Build Tag List
  foreach($data as $currentRow){
    
     //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
      if(isset($currentRow[$tagNameRow])){
        if(strlen($currentRow[$tagNameRow]) > 0){   
          
          //Check for duplicate tags
          $duplicate = false;

          foreach($tagArray as $currentTag){
            if($currentTag['Name'] == $currentRow[$tagNameRow]){
              $duplicate = true;
            }
          }
          
          $tagArray[$i]['Name'] =  $currentRow[$tagNameRow];
          $tagArray[$i]['Group'] =  $currentRow[$tagGroupRow];
          $tagArray[$i]['Error'] = 0;
          $tagArray[$i]['Message'] = "";

          

          if($duplicate){
            $tagArray[$i]['Error'] = 1;
            $tagArray[$i]['Message'] .= "Duplicate Tag";
          }

          ///Check That Tag Group Exists In Tag Group List
          $exists = false;

          if(strlen($tagArray[$i]['Group']) > 0){
            foreach($tagGroupArray as $currentTagGroup){
              if(strtolower($currentTagGroup['Name']) == strtolower($tagArray[$i]['Group'])){
                $exists = true;
              } 
            }          
          }
          else
          {
            $exists = true;
          }

          if(!$exists){
            $tagArray[$i]['Error'] = 1;
            $tagArray[$i]['Message'] .= "Tag Group Doesn't Exist";
          }


          $i = $i + 1;
        } 
    }
    }
  }
}

//Builds the Tag and Tag Group array from the data array from the excel file.
function ct_import_transactionInfo($data,&$errorstring,&$transactionArray,$tagArray, $categoryArray, $accountArray)
{

  //Tag Information Location
  $transactionDateRow = 'A';
	$transactionAccountRow = 'B';
	$transactionInvoiceNoRow = 'C';
	$transactionPayeeRow = 'D';
	$transactionMemoRow = 'E';
	$transactionAmountRow = 'F';
	$transactionCategoryRow = 'G';
	$transactionTagRow = 'H';
  
  $transactionArray = array();
  
  $i = 0;

  $firstRow = true;
  
  //Build Tag Group List
  foreach($data as $currentRow){
    
    //Skip the first row as this is the title row.
    if($firstRow){$firstRow = false;}
    else{
     
      //if(isset($currentRow[$groupNameRow])){
        if(strlen($currentRow[$transactionAmountRow]) > 0){
          
          $transactionArray[$i]['Date'] 			=  $currentRow[$transactionDateRow];
					$transactionArray[$i]['Account'] 		=  $currentRow[$transactionAccountRow];
					$transactionArray[$i]['InvoiceNo'] 	=  $currentRow[$transactionInvoiceNoRow];
					$transactionArray[$i]['Payee'] 			=  $currentRow[$transactionPayeeRow];
					$transactionArray[$i]['Memo'] 			=  $currentRow[$transactionMemoRow];
					$transactionArray[$i]['Amount'] 		=  $currentRow[$transactionAmountRow];
					$transactionArray[$i]['Category'] 	=  $currentRow[$transactionCategoryRow];
					$transactionArray[$i]['Tag'] 				=  $currentRow[$transactionTagRow];

          $transactionArray[$i]['Error'] = 0;
					$transactionArray[$i]['Message'] = "";

         $i = $i + 1;
        }
    }
  }
  
  
}
  

?>