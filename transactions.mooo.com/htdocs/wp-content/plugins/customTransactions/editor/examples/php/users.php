<?php

// DataTables PHP library
include( "../Editor/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
	
$userid = 1;

if ( ! isset($_POST['site']) || ! is_numeric($_POST['site']) ) {
	echo json_encode( [ "data" => [] ] );
}
else {
	Editor::inst( $db, 'TagGroups' )
		->field( 
			Field::inst( 'TagGroups.name' )
		)
		->where( 'user_id', $userid )
		->process($_POST)
		->json();
}
