<?php

error_reporting(E_ALL); 
ini_set('display_errors', 1);

$ct_nameArray = array(
	array("arrayName" => "DisplayTypeSelection", "getName" => "ct_DisplayTypeSelection", "defaultValue" => 0),
	array("arrayName" => "IncludeAccountFilterType", "getName" => "ct_filterTypeAccountCheckbox", "defaultValue" => false),
	array("arrayName" => "IncludeTagFilterType", "getName" => "ct_filterTypeTagCheckbox", "defaultValue" => false),
	array("arrayName" => "IncludeCatFilterType", "getName" => "ct_filterTypeCatCheckbox", "defaultValue" => false),
	array("arrayName" => "IncludeTransFilterType", "getName" => "ct_filterTypeTransCheckbox", "defaultValue" => false),
	array("arrayName" => "AccountSelectionInvert", "getName" => "ct_AccountSelectionInvert", "defaultValue" => false),
  array("arrayName" => "AccountSelection", "getName" => "ct_AccountSelection", "defaultValue" => array()),
	array("arrayName" => "AccountTypeSelectionInvert", "getName" => "ct_AccountTypeSelectionInvert", "defaultValue" => false),
  array("arrayName" => "AccountTypeSelection", "getName" => "ct_AccountTypeSelection", "defaultValue" => array()),
	array("arrayName" => "AccountGroupSelectionInvert", "getName" => "ct_AccountGroupSelectionInvert", "defaultValue" => false),
	array("arrayName" => "AccountGroupSelection", "getName" => "ct_AccountGroupSelection", "defaultValue" => array()),
	array("arrayName" => "AccountGroupOtherSelectionInvert", "getName" => "ct_AccountOtherGroupSelectionInvert", "defaultValue" => false),
	array("arrayName" => "AccountGroupOtherSelection", "getName" => "ct_AccountOtherGroupSelection", "defaultValue" => array()),
	array("arrayName" => "AccountOptionsCheckbox", "getName" => "ct_AccountOptionsCheckbox", "defaultValue" => array()),
	array("arrayName" => "TagSelectionInvert", "getName" => "ct_TagSelectionInvert", "defaultValue" => false),
	array("arrayName" => "TagSelection", "getName" => "ct_TagSelection", "defaultValue" => array()),
	array("arrayName" => "TagGroupSelectionInvert", "getName" => "ct_TagGroupSelectionInvert", "defaultValue" => false),
	array("arrayName" => "TagGroupSelection", "getName" => "ct_TagGroupSelection", "defaultValue" => array()),
	array("arrayName" => "CatSelectionInvert", "getName" => "ct_CatSelectionInvert", "defaultValue" => false),
	array("arrayName" => "CatSelection", "getName" => "ct_CatSelection", "defaultValue" => array()),
	array("arrayName" => "CatTypeSelectionInvert", "getName" => "ct_CatTypeSelectionInvert", "defaultValue" => false),
  array("arrayName" => "CatTypeSelection", "getName" => "ct_CatTypeSelection", "defaultValue" => array()),
	array("arrayName" => "CatGroupSelectionInvert", "getName" => "ct_CatGroupSelectionInvert", "defaultValue" => false),
	array("arrayName" => "CatGroupSelection", "getName" => "ct_CatGroupSelection", "defaultValue" => array()),
	array("arrayName" => "CatGroupOtherSelectionInvert", "getName" => "ct_CatGroupOtherSelectionInvert", "defaultValue" => false),
	array("arrayName" => "CatGroupOtherSelection", "getName" => "ct_CatGroupOtherSelection", "defaultValue" => array()),
	array("arrayName" => "DateSelection", "getName" => "ct_DateSelection", "defaultValue" => 0),
	array("arrayName" => "DateStart", "getName" => "ct_DateStart", "defaultValue" => 0),
	array("arrayName" => "DateEnd", "getName" => "ct_DateEnd", "defaultValue" => 0),
	array("arrayName" => "NumberOfDays", "getName" => "ct_NumberDays", "defaultValue" => 10000000),
	array("arrayName" => "MinimumAmount", "getName" => "ct_MinimumAmount", "defaultValue" => -1000000000),
	array("arrayName" => "MaximumAmount", "getName" => "ct_MaximumAmount", "defaultValue" => 1000000000)
	);

function ct_register_script(){
	wp_register_style( 'ct_css_jquery_ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
	wp_register_style( 'ct_css_jquery_dataTables', 'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css' );
	wp_register_style( 'ct_css_responsive_dataTables', 'https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css');
	wp_register_style( 'ct_css_buttons_dataTables', 'https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css' );
	wp_register_style( 'ct_css_select_dataTables', 'https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css' );
	wp_register_style( 'ct_css_keytable_dataTables', 'https://cdn.datatables.net/keytable/2.1.2/css/keyTable.dataTables.min.css' );
	wp_register_style( 'ct_css_autofill_dataTables', 'https://cdn.datatables.net/autofill/2.1.2/css/autoFill.dataTables.min.css' );
	wp_register_style( 'ct_css_editor_dataTables', plugins_url('customTransactions/editor/css/editor.dataTables.min.css') );
	wp_register_style( 'ct_css_select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css');
	
	wp_register_script( 'ct_jquery', '//code.jquery.com/jquery-1.12.3.min.js');
	wp_register_script( 'ct_jquery_ui', '//code.jquery.com/ui/1.12.1/jquery-ui.js');
	wp_register_script( 'ct_js_dataTables', 'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js');
	wp_register_script( 'ct_js_responsive_dataTables', 'https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js');
	wp_register_script( 'ct_js_buttons_dataTables', 'https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js');
	wp_register_script( 'ct_js_select_dataTables', 'https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js');
	wp_register_script( 'ct_js_keytable_dataTables', 'https://cdn.datatables.net/keytable/2.1.2/js/dataTables.keyTable.min.js');
	wp_register_script( 'ct_js_autofill_dataTables', 'https://cdn.datatables.net/autofill/2.1.2/js/dataTables.autoFill.min.js');
	wp_register_script( 'ct_js_editor_dataTables',  plugins_url('customTransactions/editor/js/dataTables.editor.min.js') );
	wp_register_script( 'ct_js_select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js');
}

function ct_getTransactionFilters(){
	
		$resultArray = array();
	
		//Get Selected Accounts
		if(isset($_GET['ct_AccountSelection'])){$resultArray['Accounts'] = $_GET['ct_AccountSelection'];}
		else{$resultArray['Accounts'] = array("-1");}
	
		//Get Selected Tags
		if(isset($_GET['ct_TagSelection'])){	$resultArray['Tags'] = $_GET['ct_TagSelection'];	}
		else{$resultArray['Tags'] = array("-1");}
				 
		return $resultArray;
	
}

function ct_enqueue_scripts(){
	wp_enqueue_style( 'ct_css_jquery_ui');
	wp_enqueue_style( 'ct_css_jquery_dataTables' );
	wp_enqueue_style( 'ct_css_responsive_dataTables');
	wp_enqueue_style( 'ct_css_buttons_dataTables' );
	wp_enqueue_style( 'ct_css_select_dataTables' );
	wp_enqueue_style( 'ct_css_keytable_dataTables' );
	wp_enqueue_style( 'ct_css_autofill_dataTables' );
	wp_enqueue_style( 'ct_css_editor_dataTables' );
	wp_enqueue_style( 'ct_css_customTransactions');
	
	wp_enqueue_script( 'ct_jquery');
	wp_enqueue_script( 'ct_jquery_ui');
	wp_enqueue_script( 'ct_js_dataTables');
	wp_enqueue_script( 'ct_js_responsive_dataTables');
	wp_enqueue_script( 'ct_js_buttons_dataTables');
	wp_enqueue_script( 'ct_js_select_dataTables');
	wp_enqueue_script( 'ct_js_keytable_dataTables');
	wp_enqueue_script( 'ct_js_autofill_dataTables');
	wp_enqueue_script( 'ct_js_editor_dataTables');
	wp_enqueue_script( 'ct_js_selectize');
	
	wp_enqueue_style( 'ct_css_select2');
	wp_enqueue_script('ct_js_select2');
}

//Function to print shortcode of transactions selection form
function ct_shortcode_transactionsform(){
	
	//Build and validate the selection form
	ct_transactionsform($isvalid,$tablestring,$selectionarray);
	
	//$additionalString = print_r($selectionarray,TRUE);
	$additionalString = "";
	
	return $tablestring.$additionalString;
}

//Generates the form HTML as well as processing the submitted form for the
//transaction selection.
function ct_transactionsform(&$isvalid,&$tablestring,&$selectionarray){
	
	$prestring = "";
	$formDateFormatjavascript = "yy-mm-dd";
	$formDateFormatPHP = "Y-m-d";
	
	//Get User Id and only generate the table if a user is logged in .
	if(is_user_logged_in()){
		$userid = get_current_user_id();
	
		ct_getAccountInfo($userid);
	
		// instantiate a Zebra_Form object
		$form = new Zebra_Form('ct_translist_configform','GET');

		//Disable CSRF - This is done as he form doesn't edit anything, and other validation will take place.
		$form->csrf(FALSE);
		
		//Set to show all error messages
		$form->show_all_error_messages(true);

		//Add Display Type Selection
		$displaySelectionOptions = array(
									'0' => 'Transactions',
									'1' => 'Pivot Table'
		);


		$form->add('label','label_catDisplayTypeSelection','ct_DisplayTypeSelection','Display Type');
		$displaySelectionObj = $form->add('select', 'ct_DisplayTypeSelection', 'CatDisplayTypeSelection', array('class' => 'form_ct_DisplayTypeSelection'));
		$displaySelectionObj->add_options($displaySelectionOptions);

		//Add Filter Type (Account) Enable Checkbox
		$form->add('label','label_filterTypeAccountCheckbox','ct_filterTypeAccountCheckbox','Use account filters');
		$form->add('checkbox','ct_filterTypeAccountCheckbox','true','', array('class' => 'btn-group'));

		//Add Filter Type (Tag) Enable Checkbox
		$form->add('label','label_filterTypeTagCheckbox','ct_filterTypeTagCheckbox','Use tag filters');
		$form->add('checkbox','ct_filterTypeTagCheckbox','true','', array('class' => 'btn-group'));

		//Add Filter Type (Category) Enable Checkbox
		$form->add('label','label_filterTypeCatCheckbox','ct_filterTypeCatCheckbox','Use category filters');
		$form->add('checkbox','ct_filterTypeCatCheckbox','true','', array('class' => 'btn-group'));

			//Add Filter Type (Transaction) Enable Checkbox
		$form->add('label','label_filterTypeTransCheckbox','ct_filterTypeTransCheckbox','Use transaction filters');
		$form->add('checkbox','ct_filterTypeTransCheckbox','true','', array('class' => 'btn-group'));

		//Add Account Invert Option
		$form->add('label','label_accountSelectionInvert','ct_AccountSelectionInvert','Exclude Selected Accounts');
		$form->add('checkbox','ct_AccountSelectionInvert','true');

		//Add Account Selection Box
		$form->add('label','label_accountSelection','ct_AccountSelection[]','Account(s)');
		$accountsObj = $form->add('select', 'ct_AccountSelection[]', 'Accounts', array('multiple' => 'multiple','class' => 'form_ct_AccountSelection'));
		$accountsObj->add_options(ct_get_accounts($userid,FALSE));

		//Add Account Type Invert Option
		$form->add('label','label_accountTypeSelectionInvert','ct_AccountTypeSelectionInvert','Exclude Selected Account Types');
		$form->add('checkbox','ct_AccountTypeSelectionInvert','true');

		//Add Account Type Selection Box
		$form->add('label','label_accountTypeSelection','ct_AccountTypeSelection[]','Account Type(s)');
		$accountTypesObj = $form->add('select', 'ct_AccountTypeSelection[]', 'AccountTypes', array('multiple' => 'multiple','class' => 'form_ct_AccountTypeSelection'));
		$accountTypesObj->add_options(ct_get_accounttypes($userid,FALSE));

		//Add Account Group Invert Option
		$form->add('label','label_accountGroupSelectionInvert','ct_AccountGroupSelectionInvert','Exclude Selected Account Groups');
		$form->add('checkbox','ct_AccountGroupSelectionInvert','true');

		//Add Account Group Selection Box
		$form->add('label','label_accountGroupSelection','ct_AccountGroupSelection[]','Account Group(s)');
		$accountGroupsObj = $form->add('select', 'ct_AccountGroupSelection[]', 'AccountGroups', array('multiple' => 'multiple','class' => 'form_ct_AccountGroupSelection'));
		$accountGroupsObj->add_options(ct_get_accountgroups($userid,FALSE));

		//Add Account Other Group Invert Option
		$form->add('label','label_accountOtherGroupSelectionInvert','ct_AccountOtherGroupSelectionInvert','Exclude Selected Account Other Groups');
		$form->add('checkbox','ct_AccountOtherGroupSelectionInvert','true');

		//Add Account Other Group Selection Box
		$form->add('label','label_accountOtherGroupSelection','ct_AccountOtherGroupSelection[]','Account Other Group(s)');
		$accountOtherGroupsObj = $form->add('select', 'ct_AccountOtherGroupSelection[]', 'AccountOtherGroups', array('multiple' => 'multiple','class' => 'form_ct_AccountOtherGroupSelection'));
		$accountOtherGroupsObj->add_options(ct_get_accountothergroups($userid,FALSE));

		//Add Account In Categories Checkboxes
		$form->add('label','label_accountOptionsCheckbox','ct_accountOptionsCheckbox','Include Account If');
		$form->add('checkboxes','ct_AccountOptionsCheckbox[]',array('ct_AcctInCash' => 'In Cash','ct_AcctNotInCash' => 'Not In Cash','ct_AcctInNetWorth' => 'In Net Worth','ct_AcctNotInNetWorth' => 'Not In Net Worth'),'', array('class' => 'btn-group'));

		//Add Tag Invert Option
		$form->add('label','label_tagSelectionInvert','ct_tagSelectionInvert','Exclude Selected Tags');
		$form->add('checkbox','ct_TagSelectionInvert','true');

		//Add Tag Selection Box
		$form->add('label','label_tagSelection','ct_TagSelection[]','Tag(s)');
		$tagsObj = $form->add('select', 'ct_TagSelection[]', 'Tags', array('multiple' => 'multiple','class' => 'form_ct_TagSelection'));
		$tagsObj->add_options(ct_get_tags($userid,FALSE));

		//Add Tag Group Invert Option
		$form->add('label','label_tagGroupSelectionInvert','ct_TagGroupSelectionInvert','Exclude Selected Tag Groups');
		$form->add('checkbox','ct_TagGroupSelectionInvert','true');

		//Add Tag Group Selection Box
		$form->add('label','label_tagGroupSelection','ct_TagGroupSelection[]','Tag Group(s)');
		$tagGroupObj = $form->add('select', 'ct_TagGroupSelection[]', 'TagGroups', array('multiple' => 'multiple','class' => 'form_ct_TagGroupSelection'));
		$tagGroupObj->add_options(ct_get_taggroups($userid,FALSE));


		//Add Category Invert Option
		$form->add('label','label_catSelectionInvert','ct_CatSelectionInvert','Exclude Selected Categories');
		$form->add('checkbox','ct_CatSelectionInvert','true');

		//Add Category Selection Box
		$form->add('label','label_catSelection','ct_CatSelection[]','Category(s)');
		$catsObj = $form->add('select', 'ct_CatSelection[]', 'Categories', array('multiple' => 'multiple','class' => 'form_ct_CatSelection'));
		$catsObj->add_options(ct_get_cats($userid,FALSE));

		//Add Category Type Invert Option
		$form->add('label','label_catTypeSelectionInvert','ct_CatTypeSelectionInvert','Exclude Selected Category Types');
		$form->add('checkbox','ct_CatTypeSelectionInvert','true');

		//Add Category Type Selection Box
		$form->add('label','label_catTypeSelection','ct_CatTypeSelection[]','Category Type(s)');
		$catTypesObj = $form->add('select', 'ct_CatTypeSelection[]', 'CatTypes', array('multiple' => 'multiple','class' => 'form_ct_CatTypeSelection'));
		$catTypesObj->add_options(ct_get_cattypes($userid,FALSE));

		
		//Add Category Group Invert Option
		$form->add('label','label_catGroupSelectionInvert','ct_CatGroupSelectionInvert','Exclude Selected Category Groups');
		$form->add('checkbox','ct_CatGroupSelectionInvert','true');

		//Add Category Group Selection Box
		$form->add('label','label_catGroupSelection','ct_CatGroupSelection[]','Category Group(s)');
		$catGroupsObj = $form->add('select', 'ct_CatGroupSelection[]', 'CategoryGroups', array('multiple' => 'multiple','class' => 'form_ct_CatGroupSelection'));
		$catGroupsObj->add_options(ct_get_catgroups($userid,FALSE));

		//Add Category Other Group Invert Option
		$form->add('label','label_catGroupOtherSelectionInvert','ct_CatGroupOtherSelectionInvert','Exclude Selected Category Other Groups');
		$form->add('checkbox','ct_CatGroupOtherSelectionInvert','true');

		//Add Account Other Group Selection Box
		$form->add('label','label_catGroupOtherSelection','ct_CatGroupOtherSelection[]','Category Other Group(s)');
		$catOtherGroupsObj = $form->add('select', 'ct_CatGroupOtherSelection[]', 'CatOtherGroups', array('multiple' => 'multiple','class' => 'form_ct_CatOtherGroupSelection'));
		$catOtherGroupsObj->add_options(ct_get_catothergroups($userid,FALSE));

		//Add Date Selection
		$dateSelectionOptions = array(
									'0' => 'All Dates',
									'1' => 'Fixed Dates',
									'2' => 'Previous x Days',
									'3' => 'Last Month',
									'4' => 'Last Three Months',
									'5' => 'Last Six Months',
									'6' => 'Last Twelve Months',
									'7' => 'Current Month',
									'8' => 'Previous Month',
									'9' => 'Current Quarter',
									'10' => 'Previous Quarter',
									'11' => 'Current Calendar Year',
									'12' => 'Previous Calendar Year',
									'13' => 'Current Financial Year',
									'14' => 'Previous Financial Year'	
		);

		$form->add('label','label_catDateSelection','ct_DateSelection','Date Selection');
		$dateSelectionObj = $form->add('select', 'ct_DateSelection', 'CatDateSelection', array('class' => 'form_ct_DateSelection'));
		$dateSelectionObj->add_options($dateSelectionOptions);

		//Add Start Date
		$form->add('label','label_catDateStart','ct_DateStart','Start Date');
		$startDateObj = $form->add('date', 'ct_DateStart','',array('class' => 'form_ct_DateStart'));
		$startDateObj->set_rule(array(
			'date'  =>  array('error', 'Invalid date specified!'),
		));
		// set the date's format
		$startDateObj->format($formDateFormatPHP);

		//Add End Date
		$form->add('label','label_catDateEnd','ct_DateEnd','End Date');
		$endDateObj = $form->add('date', 'ct_DateEnd','',array('class' => 'form_ct_DateEnd'));
		$endDateObj->set_rule(array(
			'date'  =>  array('error', 'Invalid date specified!'),
		));
		// set the date's format
		$endDateObj->format($formDateFormatPHP);

		//Add Number of Days
		$form->add('label','label_catNumberDays','ct_NumberDays','Number of Days Back to Include');
		$numberDaysObj = $form->add('text','ct_NumberDays');
		$numberDaysObj->set_rule(array('number' => array('', 'error', 'Number of Days is an Invalid Number')));

		//Add Amount Filter (minimum amount)
		$form->add('label','label_catMinimumAmount','ct_MinimumAmount','Minimum Transaction Value');
		$minimumAmountObj = $form->add('text','ct_MinimumAmount');
		$minimumAmountObj->set_rule(array('number' => array('', 'error', 'Minimum Amount is not a number')));

		//Add Amount Filter (maximum amount)
		$form->add('label','label_catMaxiumumAmount','ct_MaximumAmount','Maximum Transaction Value');
		$maximumAmountObj = $form->add('text','ct_MaximumAmount');
		$maximumAmountObj->set_rule(array('number' => array('', 'error', 'Maximum Amount is not a number')));

		// "submit"
		$form->add('submit', 'btnsubmit', 'Submit');

		// validate the form
		if ($form->validate()) {

			// do stuff here
			$filterArray = ct_transactions_arrayfromget();

			//Update data to be returned by function
			$selectionarray = $filterArray;
			$isvalid = true;

		}
		else
		{

			//Update data to be returned by function
			$selectionarray = array();
			$isvalid = false;
		}

		// auto generate output, labels above form elements
		$formstring = $form->render("*horizontal",TRUE);

		//Initialise strings to build up script and form component
		$scriptstring = '<script type="text/javascript">
				$(document).ready(function() {
						$(".form_ct_AccountSelection").select2();
						$(".form_ct_AccountTypeSelection").select2();
						$(".form_ct_AccountGroupSelection").select2();
						$(".form_ct_AccountOtherGroupSelection").select2();
						$(".form_ct_TagSelection").select2();
						$(".form_ct_TagGroupSelection").select2();
						$(".form_ct_CatSelection").select2();
						$(".form_ct_CatTypeSelection").select2();
						$(".form_ct_CatGroupSelection").select2();
						$(".form_ct_CatOtherGroupSelection").select2();
						$(".form_ct_DateSelection").select2();
						$(".form_ct_DisplayTypeSelection").select2();
						$(".form_ct_DateStart").datepicker({ dateFormat: \''.$formDateFormatjavascript.'\' });
						$(".form_ct_DateEnd").datepicker({ dateFormat: \''.$formDateFormatjavascript.'\' });
						});

		</script>';
		//Create Final String;
		$string = $prestring . $scriptstring . $formstring;

		$tablestring = $string;
		
	}
	//If the user is not logged in, generate an error message.
	else{
		$userid = -1;
		
		//Update data to be output from the function
		$tablestring = "User Not Logged In";
		$selectionarray = array();
		$isvalid = false;
		
		return $results;
	}
}

function ct_transactions_arrayfromget(){
	//Setup array of source array keys and destination array keys. THis is used to map only some of the $_GET variables to the filtering variables.
	global $ct_nameArray;
	$nameArray = $ct_nameArray;	

	$array = array();
	
	foreach($nameArray as $currentItem)
	{
		if(isset($_GET[$currentItem['getName']])){
			$array[$currentItem['arrayName']] = $_GET[$currentItem['getName']];
		}
		else{
		//	$array[$currentItem['arrayName']] = $currentItem['defaultValue'];
		}
	}

	return $array;
}

function ct_getAccountInfo($userid){
	
	$resultArray = array();
	
	$resultArray['Accounts'] = ct_get_accounts($userid);
	$resultArray['AccountOtherGroups'] = ct_get_accountothergroups($userid);
	$resultArray['AccountGroups'] = ct_get_accountgroups($userid);
	$resultArray['AccountTypes'] = ct_get_accounttypes($userid);
	
	//print_r($resultArray);
	
	return $resultArray;
		
}

function ct_process_resultsArray($array){
	$result = array();
	
	foreach($array as $currentItem){
		$result[$currentItem['GroupName']][$currentItem['ID']] = $currentItem['Name'];
	}
		
	return $result;

	
}

function ct_get_accounts($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select Accounts.Name as Name, AccountGroups.name as GroupName, Accounts.Id as ID
	from Accounts 
	left join AccountGroups on Accounts.AccountGroup = AccountGroups.id
	where Accounts.user_id = '.$userid.'
	order by GroupName, Name ASC', ARRAY_A  );
	
	//Reformat Results
	$results = ct_process_resultsArray($sqlresults);

	return $results;
}

function ct_get_accountothergroups($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select distinct Accounts.OtherGrouping as Grouping
	from Accounts 
	where Accounts.user_id = '.$userid.'
	order by Grouping ASC', ARRAY_A  );
	
	$results = array();
	$i = 0;
	foreach($sqlresults as $currentresult){
		if(strlen($currentresult['Grouping']) > 0){
			$results[$currentresult['Grouping']] = $currentresult['Grouping'];
			$i = $i + 1;
		}
	}

	return $results; 
}

function ct_get_accounttypes($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select AccountTypes.Name as Name, AccountTypes.Id as ID
	from AccountTypes 
	order by Name ASC', ARRAY_A  );	
	
	$results = array();
	
	foreach($sqlresults as $currentresult){
		$results[$currentresult['ID']] = $currentresult['Name'];
	}
	
	return $results;
}

function ct_get_accountgroups($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select AccountGroups.name as Name, AccountGroups.id as ID
	from AccountGroups 
	where AccountGroups.user_id = '.$userid.'
	order by Name ASC', ARRAY_A  );
	
	$results = array();
	
	foreach($sqlresults as $currentresult){
		$results[$currentresult['ID']] = $currentresult['Name'];
	}
	return $results;
}

function ct_get_tags($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select Tags.Name as Name, TagGroups.name as GroupName, Tags.ID as ID
	from Tags 
	left join TagGroups on Tags.tag_Group = TagGroups.id
	where Tags.user_id = '.$userid.'
	order by GroupName, Name ASC', ARRAY_A  );
	
	//Reformat Results
	$results = ct_process_resultsArray($sqlresults);
	
	return $results;
}

function ct_get_taggroups($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select TagGroups.name as Name, TagGroups.id as ID
	from TagGroups 
	where TagGroups.user_id = '.$userid.'
	order by Name ASC', ARRAY_A  );
	
	$results = array();
	
	foreach($sqlresults as $currentresult){
		$results[$currentresult['ID']] = $currentresult['Name'];
	}
	return $results;
}

function ct_get_cats($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select Categories.Name as Name, CategoryGroups.name as GroupName, Categories.Id as ID
	from Categories 
	left join CategoryGroups on Categories.CategoryGroup = CategoryGroups.id
	where Categories.user_id = '.$userid.'
	order by GroupName, Name ASC', ARRAY_A  );
	
	//Reformat Results
	$results = ct_process_resultsArray($sqlresults);
	
	return $results;
}

function ct_get_catgroups($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select CategoryGroups.name as Name, CategoryGroups.id as ID
	from CategoryGroups 
	where CategoryGroups.user_id = '.$userid.'
	order by Name ASC', ARRAY_A  );
	
	$results = array();
	
	foreach($sqlresults as $currentresult){
		$results[$currentresult['ID']] = $currentresult['Name'];
	}
	return $results;
}

function ct_get_catothergroups($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select distinct Categories.othergrouping as Grouping
	from Categories
	where Categories.user_id = '.$userid.'
	order by Grouping ASC', ARRAY_A  );
	
	$results = array();
	$i = 0;
	foreach($sqlresults as $currentresult){
		if(strlen($currentresult['Grouping']) > 0){
			$results[$currentresult['Grouping']] = $currentresult['Grouping'];
			$i = $i + 1;
		}
	}
	
	return $results;
}

function ct_get_cattypes($userid,$includeTotals = FALSE){
	global $wpdb;
	$sqlresults = $wpdb->get_results( 'select CategoryTypes.name as Name, CategoryTypes.id as ID
	from CategoryTypes 
	order by Name ASC', ARRAY_A  );	
	
	$results = array();
	
	foreach($sqlresults as $currentresult){
		$results[$currentresult['ID']] = $currentresult['Name'];
	}
	
	return $results;
}


function ct_shortcode_tagstable(){
	
	wp_register_script( 'ct_js_tagTables', plugins_url('customTransactions/tagsTable.js'));
	wp_localize_script( 'ct_js_tagTables', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'ct_js_tagTables');
	$result =  '<table id="taggroups_table" class="display" cellspacing="0" width="100%">
					<tr>
						<th></th>
						<th>Name</th>
					</tr>
			</table>
			
			<table id="tags_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Tag Group</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>';
	
	return $result;
	
	
}

function ct_shortcode_categoriestable(){
	
	wp_register_script( 'ct_js_categoryTables', plugins_url('customTransactions/categoriesTable.js'));
	wp_localize_script( 'ct_js_categoryTables', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'ct_js_categoryTables');
	$result =  '<table id="categorygroups_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
					</tr>

				</thead>

			</table>
			
			<table id="categories_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Category Group</th>
						<th>Category Type</th>
						<th>Other Grouping</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>';
	
	return $result;
	
	
}

function ct_shortcode_accountstable(){
	
	wp_register_script( 'ct_js_accountTables', plugins_url('customTransactions/accountsTable.js'));
	wp_localize_script( 'ct_js_accountTables', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	wp_enqueue_script( 'ct_js_accountTables');
	$result =  '<table id="accountgroups_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
					</tr>

				</thead>

			</table>
			
			<table id="accounts_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Account Group</th>
						<th>Account Type</th>
						<th>Other Grouping</th>
						<th>In Cash</th>
						<th>In Net Worth</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>';
	
	return $result;
	
	
}

function display_transactionstable(){
	
	wp_register_script( 'ct_js_transactionTables', plugins_url('customTransactions/transactionsTable2.js'));
	$httpquery = http_build_query($_GET);
	$url = admin_url( 'admin-ajax.php' ) ."?". $httpquery;
	wp_localize_script( 'ct_js_transactionTables', 'ajax_object', array( 'ajax_url' => $url )  );
	wp_enqueue_script( 'ct_js_transactionTables');
	$result =  '<table id="transactions_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Date</th>
						<th>Account</th>
						<th>Invoice Number</th>
						<th>Payee</th>
						<th>Memo</th>
						<th>Amount</th>
						<th>Category</th>
						<th>Tag</th>
						<th>Total</th>
					</tr>

				</thead>

			</table>';
	
	return $result;

}

function ct_shortcode_transactionstable(){
	
	$string = display_transactionstable();
	
	return $string;

}

function ct_ajax_accounts(){

	 global $wpdb;

	// DataTables PHP library
	include( "editorPHP/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("accounts_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
	}

	if(is_user_logged_in()){
	
		$userid = get_current_user_id();
	
			\DataTables\Editor::inst( $db, 'Accounts' )
				->field( 
					\DataTables\Editor\Field::inst( 'Accounts.Name' ),
					\DataTables\Editor\Field::inst( 'Accounts.AccountGroup')
					->options('AccountGroups','id','name',function ($q)  use ($userid){
		        $q->where( 'AccountGroups.user_id', $userid );
		    })
					->validator( 'Validate::dbValues' ),
					\DataTables\Editor\Field::inst( 'Accounts.AccountType')
					->options('AccountTypes','id','name')
					->validator( 'Validate::dbValues' ),
					\DataTables\Editor\Field::inst( 'Accounts.user_id')
					->setValue( $userid ),
					\DataTables\Editor\Field::inst( 'AccountGroups.name'),
					\DataTables\Editor\FIeld::inst( 'AccountTypes.name'),
					\DataTables\Editor\Field::inst( 'Accounts.OtherGrouping'),
					\DataTables\Editor\Field::inst( 'Accounts.InCash'),
					\DataTables\Editor\Field::inst( 'Accounts.InNetWorth'),
					\DataTables\Editor\Field::inst( 'Accounts.Description')
				)
				->where( 'Accounts.user_id', $userid )
				->leftJoin('AccountGroups','AccountGroups.id','=','Accounts.AccountGroup')
				->leftJoin('AccountTypes','AccountTypes.id','=','Accounts.AccountType')
				->process($_POST)
				->json();
		
	}
	else
	{
		echo "Error : No User Logged In";
	
	}
	wp_die();
}

function ct_ajax_accountGroups(){

		global $wpdb;

	// DataTables PHP library
	include( "editorPHP/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("accountGroups_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
		
	}

if(is_user_logged_in()){

	$userid = get_current_user_id();

			\DataTables\Editor::inst( $db, 'AccountGroups' )
		->field( 
			\DataTables\Editor\Field::inst( 'name' ),
			\DataTables\Editor\Field::inst( 'user_id')
			->setValue( $userid )
		)
		->where( 'AccountGroups.user_id', $userid )
		->process($_POST)
		->json();

}
else
{
	echo "Error : No User Logged In";

}
	wp_die();
}	

function ct_ajax_tags(){

	global $wpdb;

	// DataTables PHP library
	include( "editorPHP/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("tags_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
		
	}

	if(is_user_logged_in()){
	
		$userid = get_current_user_id();
	
			\DataTables\Editor::inst( $db, 'Tags' )
			->field( 
				\DataTables\Editor\Field::inst( 'Tags.name' ),
				\DataTables\Editor\Field::inst( 'Tags.tag_Group')
				->options('TagGroups','id','name',function ($q)  use ($userid) {
	        $q->where( 'TagGroups.user_id', $userid );
	    })
				->validator( 'Validate::dbValues' ),
				\DataTables\Editor\Field::inst( 'Tags.user_id')
				->setValue( $userid ),
				\DataTables\Editor\Field::inst( 'TagGroups.name')
			)
			->where( 'Tags.user_id', $userid )
			->leftJoin('TagGroups','TagGroups.id','=','Tags.tag_Group')
			->process($_POST)
			->json();
	}
	else
	{
		echo "Error : No User Logged In";
	
	}
	wp_die();
}

function ct_ajax_tagGroups(){

		global $wpdb;

	// DataTables PHP library
	include( "editor/php/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("tagGroups_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
		
	}

	if(is_user_logged_in()){
	
		$userid = get_current_user_id();
	
		\DataTables\Editor::inst( $db, 'TagGroups' )
			->field( 
				\DataTables\Editor\Field::inst( 'name' ),
				\DataTables\Editor\Field::inst( 'user_id')
				->setValue( $userid )
			)
			->where( 'TagGroups.user_id', $userid )
			->process($_POST)
			->json();
	}
	else
	{
		echo "Error : No User Logged In";
	
	}
	wp_die();
}

function ct_ajax_categories(){

		global $wpdb;

	// DataTables PHP library
	include( "editor/php/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("categories_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
		
	}
	
	if(is_user_logged_in()){
	
		$userid = get_current_user_id();
	
			\DataTables\Editor::inst( $db, 'Categories' )
			->field( 
				\DataTables\Editor\Field::inst( 'Categories.Name' ),
				\DataTables\Editor\Field::inst( 'Categories.CategoryGroup')
				->options('CategoryGroups','id','name',function ($q)  use ($userid) {
	        $q->where( 'CategoryGroups.user_id', $userid );
	    })
				->validator( 'Validate::dbValues' ),
				\DataTables\Editor\Field::inst( 'Categories.Type')
				->options('CategoryTypes','id','name')
				->validator( 'Validate::dbValues' ),
				\DataTables\Editor\Field::inst( 'Categories.user_id')
				->setValue( $userid ),
				\DataTables\Editor\Field::inst( 'CategoryGroups.name'),
				\DataTables\Editor\Field::inst( 'CategoryTypes.name'),
				\DataTables\Editor\Field::inst( 'Categories.othergrouping'),
				\DataTables\Editor\Field::inst( 'Categories.Description')
			)
			->where( 'Categories.user_id', $userid )
			->leftJoin('CategoryGroups','CategoryGroups.id','=','Categories.CategoryGroup')
			->leftJoin('CategoryTypes','CategoryTypes.id','=','Categories.Type')
			->process($_POST)
			->json();
	}
	else
	{
		echo "Error : No User Logged In";
	
	}
	wp_die();
}

function ct_ajax_categoryGroups(){

	global $wpdb;

	// DataTables PHP library
	include( "editor/php/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("categoryGroups_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
		
		
	}
	
	if(is_user_logged_in()){
	
		$userid = get_current_user_id();
	
		\DataTables\Editor::inst( $db, 'CategoryGroups' )
		->field( 
			\DataTables\Editor\Field::inst( 'name' ),
			\DataTables\Editor\Field::inst( 'user_id')
			->setValue( $userid )
		)
		->where( 'CategoryGroups.user_id', $userid )
		->process($_POST)
		->json();
		
	}
	else
	{
		echo "Error : No User Logged In";
	
	}
	wp_die();
}

function ct_ajax_transactions(){

	global $wpdb;
	
	//set the honeypot parameter to ensure spam detection is not triggered
	$_GET['zebra_honeypot_ct_translist_configform'] = '';
	
	ct_transactionsform($form_isvalid,$form_tablestring,$form_selectionarray);
	
	$filters = $form_selectionarray;

	unset($form_tablestring);
	unset($form_selectionarray);
	
	if(!$form_isvalid and count($filters) > 0)
	{
		$formError = true;
	}
	else
	{
		$formError = false;
	}
	
	// DataTables PHP library
	include( "editor/php/DataTables.php" );

	if(isset($_POST['action'])){
		
		$_POST['action'] = str_ireplace("transactions_","",$_POST['action']);
		$_POST['action'] = str_ireplace("undefined","",$_POST['action']);
				
	}
	
	if(is_user_logged_in() and !$formError){
	
		$userid = get_current_user_id();
	
		$editor = \DataTables\Editor::inst( $db, 'Transactions' )
		->field( 
			\DataTables\Editor\Field::inst( 'Transactions.Date' )
			->validator( '\DataTables\Editor\Validate::dateFormat', array(
                "format"  => \DataTables\Editor\Format::DATE_ISO_8601,
                "message" => "Please enter a date in the format yyyy-mm-dd"
            ) )
            ->getFormatter( '\DataTables\Editor\Format::date_sql_to_format', \DataTables\Editor\Format::DATE_ISO_8601 )
            ->setFormatter( '\DataTables\Editor\Format::date_format_to_sql', \DataTables\Editor\Format::DATE_ISO_8601 ),
			\DataTables\Editor\Field::inst( 'Transactions.user_id')
			->setValue( $userid ),
			\DataTables\Editor\Field::inst( 'Transactions.Payee' ),
			\DataTables\Editor\Field::inst( 'Transactions.Memo' ),
			\DataTables\Editor\Field::inst( 'Transactions.InvoiceNo' ),
			\DataTables\Editor\Field::inst( 'Transactions.Amount' )->validator( '\DataTables\Editor\Validate::numeric' ),
			\DataTables\Editor\Field::inst( 'Accounts.Name' ),
			\DataTables\Editor\Field::inst( 'AccountGroups.Name' ),
			\DataTables\Editor\Field::inst( 'AccountTypes.name' ),
			\DataTables\Editor\Field::inst( 'Accounts.OtherGrouping'),
			\DataTables\Editor\Field::inst( 'Categories.Name' ),
			\DataTables\Editor\Field::inst( 'CategoryGroups.Name' ),
			\DataTables\Editor\Field::inst( 'CategoryTypes.name'),
			\DataTables\Editor\Field::inst( 'Categories.othergrouping'),
			\DataTables\Editor\Field::inst( 'Tags.Name' ),
			\DataTables\Editor\Field::inst( 'Tags.ID' ),
			\DataTables\Editor\Field::inst( 'TagGroups.Name' ),
			\DataTables\Editor\Field::inst( 'Transactions.Category' )
			->options('Categories','id','Name',function ($q)  use ($userid) {
	        $q->where( 'Categories.user_id', $userid );
	    }),
			\DataTables\Editor\Field::inst( 'Transactions.Tag' )
			->options('Tags','id','Name',function ($q)  use ($userid) {
	        $q->where( 'Tags.user_id', $userid );
	    }),
			\DataTables\Editor\Field::inst( 'Transactions.Account' )
			->options('Accounts','id','Name',function ($q)  use ($userid) {
	        $q->where( 'Accounts.user_id', $userid );
	    })
		)
		->where( 'Transactions.user_id', $userid );
		
		ct_datatables_addfilters($editor,$userid,$filters);
				
		$editor->leftJoin('Tags','Tags.id','=','Transactions.Tag')
		->leftJoin('Accounts','Accounts.id','=','Transactions.Account')
		->leftJoin('Categories','Categories.id','=','Transactions.Category')
		->leftJoin('CategoryGroups','CategoryGroups.id','=','Categories.CategoryGroup')
		->leftJoin('CategoryTypes','CategoryTypes.id','=','Categories.Type')
		->leftJoin('TagGroups','TagGroups.id','=','Tags.tag_Group')
		->leftJoin('AccountGroups','AccountGroups.id','=','Accounts.AccountGroup')
		->leftJoin('AccountTypes','AccountTypes.id','=','Accounts.AccountType');
				
		
	//	$editor->where('Tags.ID',"5");
		
		$editor->process($_POST)
		->json();
		
	}
	else
	{
		if($formError){	echo '{"data" : [] , "error" : "Invalid Form Submission"}';	}
		
		if(!is_user_logged_in()){	echo '{"data" : [] , "error" : "No User Logged In"}';	}
	}
	wp_die();
}

function ct_datatables_addfilters(&$editor,$userid,$filters){

	//echo "Filters: <BR>";
	
	//print_r($filters);
	
	//echo "<BR><BR>";
	
	
	//Account Filters
	if(isset($filters['IncludeAccountFilterType'])){
		if($filters['IncludeAccountFilterType'] == true){
			//echo "Including Account Filters<BR>";
			
			//Account Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'AccountSelection','AccountSelectionInvert',"=",'Transactions.Account');

			//Account Type Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'AccountTypeSelection','AccountTypeSelectionInvert',"=",'Accounts.AccountType');

			//Account Group Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'AccountGroupSelection','AccountGroupSelectionInvert',"=",'Accounts.AccountGroup');

			//Account Other Group Seleciton Filter
			ct_datatables_addfilter_group($editor,$filters,'AccountGroupOtherSelection','AccountGroupOtherSelectionInvert',"=",'Accounts.OtherGrouping');
 		
			//Account True / False Settings
			if(isset($filters['AccountOptionsCheckbox'])){
				
				$inCash = false;
				$notinCash = false;
				$inNetWorth = false;
				$notinNetWorth = false;
				
				foreach($filters['AccountOptionsCheckbox'] as $currentCheckboxFilter){
					if($currentCheckboxFilter == 'ct_AcctInCash'){ $inCash = true;}
					if($currentCheckboxFilter == 'ct_AcctNotInCash'){ $notinCash = true;}
					if($currentCheckboxFilter == 'ct_AcctInNetWOrth'){ $inNetWorth = true;}
					if($currentCheckboxFilter == 'ct_AcctNotInNetWorth'){ $notinNetWorth = true;}
				}
								
				if($inCash AND !$notinCash){
					$editor->where('Accounts.InCash','1','=');
				}
				
				if(!$inCash AND $notinCash){
					$editor->where('Accounts.InCash','0','=');
				}
				
				if($inNetWorth AND !$notinNetWorth){
					$editor->where('Accounts.InNetWorth','1','=');
				}
				
				if(!$inNetWorth AND $notinNetWorth){
					$editor->where('Accounts.InNetWorth','0','=');
				}
				
			}
		}
	}	
	
	//Tag Filters
	if(isset($filters['IncludeTagFilterType'])){
		if($filters['IncludeTagFilterType'] == true){
			//echo "Including Tag Filters<BR>";
			
			//Tag Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'TagSelection','TagSelectionInvert',"=",'Transactions.Tag');

			//Tag Group Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'TagGroupSelection','TagGroupSelectionInvert',"=",'Tags.tag_Group');
		}
	}	
	
	//Category Filters
	if(isset($filters['IncludeCatFilterType'])){
		if($filters['IncludeCatFilterType'] == true){
			//echo "Including Category Filters<BR>";
			
			//Category Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'CatSelection','CatSelectionInvert',"=",'Transactions.Category');

			//Category Type Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'CatTypeSelection','CatTypeSelectionInvert',"=",'Categories.Type');

			//Category Group Selection Filter
			ct_datatables_addfilter_group($editor,$filters,'CatGroupSelection','CatGroupSelectionInvert',"=",'Categories.CategoryGroup');

			//Category Other Group Seleciton Filter
			ct_datatables_addfilter_group($editor,$filters,'CatGroupOtherSelection','CatGroupOtherSelectionInvert',"=",'Categories.othergrouping');
 		}
	}	
	
	//Transaction Filters
	if(isset($filters['IncludeTransFilterType'])){
		if($filters['IncludeTransFilterType'] == true){
			
			//Date Selection
			if(strlen($filters['DateSelection']) > 0){
				
				//print_r($filters);
				
				if(isset($filters['DateStart']) AND strlen($filters['DateStart']) > 0){
					$dateStart = date_format(date_create($filters['DateStart']),"Y-m-d");
					}
				else {
					$dateStart = "1900-01-01";
				}
				
				if(isset($filters['DateEnd']) AND strlen($filters['DateEnd']) > 0){
					$dateEnd = date_format(date_create($filters['DateEnd']),"Y-m-d");
				}
				else {
					$dateEnd = "2100-01-01";
				}
				
				if(isset($filters['NumberOfDays'])){
					$NoDays = $filters['NumberOfDays'];
				}
				else{
					$NoDays = 365;
				}
				
				$includeFutureTransactions = false;
				$firstMonthFinYear = 4;
				
				ct_datatables_addfilter_dates($editor,$filters['DateSelection'],$includeFutureTransactions,$NoDays,$firstMonthFinYear,$dateStart,$dateEnd);
		}
			
			//Minimum Amount
			if(strlen($filters['MinimumAmount']) > 0){
				$editor->where('Transactions.Amount',$filters['MinimumAmount'],">=");
			}
			
			//Maximum Amount
			if(strlen($filters['MaximumAmount']) > 0){
				$editor->where('Transactions.Amount',$filters['MaximumAmount'],"<=");
			}
			
		}
	}
}

function ct_datatables_addfilter_dates($editor,$dateSelection,$includeFutureTransactions = false,$NoDays = 1,$firstMonthFinYear = 4,$startDateSet = "2016-10-01",$endDateSet = "2016-10-20"){
	
	//echo "Date Select : $dateSelection <BR><BR>";
	
	//Set Timezone
	date_default_timezone_set("Pacific/Auckland");
	
	//Get Current Time Details
	//$dateArray = getdate();
	$date = new DateTime();
	$date->SetTimezone(new DateTimeZone("Pacific/Auckland"));	
	
	//Setup Start and End Date Variables
	$startDate = "1900-01-01";
	$endDate = "2100-01-01";
	
	
	switch($dateSelection){
		//All Dates
		case 0:
			//Do Nothing
			break;
			
		//Fixed Dates
		case 1:
			
			$endDate = $endDateSet;
			$startDate = $startDateSet;
			
			break;
			
		//Previous x Days
		case 2:

			$endDate = $date->format("Y-m-d");
			$date->sub(new DateInterval("P".$NoDays."D"));
			$startDate = $date->format("Y-m-d");
			break;
			
		//Last Month
		case 3:
			$endDate = $date->format("Y-m-d");
			$origDay = $date->format("d");
			$date->sub(new DateInterval("P1M"));
			$finalDay = $date->format("d");
			
			//Sort Out Uneven Month Lengths to make it logically work
			if($origDay > $finalDay ){$date->modify('last day of previous month');}
			
			//Select Day (TO make interval logically make sense, 
			//the start and the end must be less than one month apart)
			$date->add(new DateInterval("P1D"));
			$startDate = $date->format("Y-m-d");
			
			break;			
			
		//Last Three Months
		case 4:
			$endDate = $date->format("Y-m-d");
			$origDay = $date->format("d");
			$date->sub(new DateInterval("P3M"));
			$finalDay = $date->format("d");
			
			//Sort Out Uneven Month Lengths to make it logically work
			if($origDay > $finalDay ){$date->modify('last day of previous month');}
			
			//Select Day (TO make interval logically make sense, 
			//the start and the end must be less than one month apart)
			$date->add(new DateInterval("P1D"));
			$startDate = $date->format("Y-m-d");
			
			break;
			
		//Last Six Months
		case 5:
			$endDate = $date->format("Y-m-d");
			$origDay = $date->format("d");
			$date->sub(new DateInterval("P6M"));
			$finalDay = $date->format("d");
			
			//Sort Out Uneven Month Lengths to make it logically work
			if($origDay > $finalDay ){$date->modify('last day of previous month');}
			
			//Select Day (TO make interval logically make sense, 
			//the start and the end must be less than one month apart)
			$date->add(new DateInterval("P1D"));
			$startDate = $date->format("Y-m-d");
			
			break;
	
		//Last Twelve Months
		case 6:
			$endDate = $date->format("Y-m-d");
			$origDay = $date->format("d");
			$date->sub(new DateInterval("P1Y"));
			$finalDay = $date->format("d");
			
			//Sort Out Uneven Month Lengths to make it logically work
			if($origDay > $finalDay ){$date->modify('last day of previous month');}
			
			//Select Day (TO make interval logically make sense, 
			//the start and the end must be less than one month apart)
			$date->add(new DateInterval("P1D"));
			$startDate = $date->format("Y-m-d");
			
			break;
			
		//Current Month To Date
		case 7:
			$endDate = $date->format("Y-m-d");
			$date->modify('first day of this month');
			$startDate = $date->format("Y-m-d");
			
			break;
			
		//Previous Month
		case 8:
			$date->modify('last day of previous month');
			$endDate = $date->format("Y-m-d");
			$date->modify('first day of this month');
			$startDate = $date->format("Y-m-d");
			
			break;
			
		//Current Quarter
		case 9:
			$endDate = $date->format("Y-m-d");
			$currentMonth = $date->format("m");
			$monthOffset = $currentMonth - (floor(($currentMonth -1)/(3)) * 3 + 1);
			$date->modify('first day of this month');
			$date->sub(new DateInterval("P".$monthOffset."M"));
			$startDate = $date->format("Y-m-d");
			
			break;

			//Previous Quarter
		case 10:
			//$endDate = $date->format("Y-m-d");
			$currentMonth = $date->format("m");
			$monthOffset = $currentMonth - (floor(($currentMonth -1)/(3)) * 3 + 1);
			$date->modify('first day of this month');
			$date->sub(new DateInterval("P".$monthOffset."M"));
			$date->sub(new DateInterval("P3M"));
			$startDate = $date->format("Y-m-d");
			$date->add(new DateInterval("P2M"));
			$date->modify('last day of this month');
			$endDate = $date->format("Y-m-d");
			
			break;
			
		//Current Calendar Year
		case 11:
			$endDate = $date->format("Y-m-d");
			$date->modify('first day of January ' . $date->format("Y"));
			$startDate = $date->format("Y-m-d");		
			
			break;
			
		//Previous Calendar Year
		case 12:
			$prevYear = $date->format("Y") - 1;
			$date->modify('first day of January ' . $prevYear);
			$startDate = $date->format("Y-m-d");	
			$date->modify('last day of December ' . $date->format("Y"));
			$endDate = $date->format("Y-m-d");	
			
			break;
			
		//Current Financial Year
		case 13:
			$endDate = $date->format("Y-m-d");
			
			if($date->format("m") < $firstMonthFinYear){ $targetYear = $date->format("Y") - 1;}
			else{ $targetYear = $date->format("Y");	}
			
			$date->setDate($targetYear,$firstMonthFinYear,1);
			$startDate = $date->format("Y-m-d");
				
			break;
			
		//Previous Financial Year
		case 14:
			
			if($date->format("m") < $firstMonthFinYear){ $targetYear = $date->format("Y") - 1;}
			else{ $targetYear = $date->format("Y");	}
			
			$date->setDate($targetYear-1,$firstMonthFinYear,1);
			$startDate = $date->format("Y-m-d");
			$date->add(new DateInterval("P1Y"));
			$date->sub(new DateInterval("P1D"));
			$endDate = $date->format("Y-m-d");
			
			break;	
	}
	
	//echo "Start Date : $startDate <BR> End Date : $endDate <BR><BR>";
	
	$editor->where('Transactions.Date',$startDate,">=");
	$editor->where('Transactions.Date',$endDate,"<=");
	
	
}

function ct_datatables_addfilter_group(&$editor,$filters,$filterName,$filterInvertName,$comparison,$columnName){
	
		if(isset($filters[$filterName])){
				if(isset($filters[$filterInvertName]))
				{ $accountInvert = true; }
				else 
				{ $accountInvert = false;}
			
				$array = $filters[$filterName];
			
				if($accountInvert){$usedComparison = "!".$comparison;}
			 else{$usedComparison = $comparison;}
	
			//Create the bracketed where component
			$editor->where( function ( $s ) use ($array,$accountInvert,$usedComparison,$columnName) {
				
				$s->where( function ( $r ) use ($array,$accountInvert,$usedComparison,$columnName) {
      	$first = true;
				//Loop thorugh all the options, the first is a normal where (i.e. AND), and the rest are OR options.
				foreach($array as $currentData){
				
					if($first){
						$r->where( $columnName, $currentData ,$usedComparison);
						//echo "COlumn Name = $columnName - Current Data = $currentData - Comparison = $usedComparison <BR>";
					}
					else{
						if(!$accountInvert){ $r->or_where( $columnName, $currentData ,$usedComparison);}
						else{ $r->where( $columnName, $currentData ,$usedComparison); 	}
					}
					$first = false;
				}
				
				
				} );
				
				//Add the null option when inverting the search to ensure any transactions without a link are also included.
				if($accountInvert){
					$s->or_where($columnName,null);
				}
				
			});
		}
	
}

function ct_ajax_transactionsForm(){
	wp_enqueue_style( 'ct_css_select2');
	wp_enqueue_script( 'ct_js_select2');
}
?>