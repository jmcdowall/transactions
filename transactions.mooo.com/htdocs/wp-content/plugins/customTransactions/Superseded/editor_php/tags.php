<?php

// DataTables PHP library
include( "../editor/php/DataTables.php" );

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
	
$userid = 1;


	Editor::inst( $db, 'Tags' )
		->field( 
			Field::inst( 'Tags.name' ),
			Field::inst( 'Tags.tag_Group')
			->options('TagGroups','id','name',function ($q) {
        $q->where( 'TagGroups.user_id', $GLOBALS['userid'] );
    })
			->validator( 'Validate::dbValues' ),
			Field::inst( 'Tags.user_id')
			->setValue( $userid ),
			Field::inst( 'TagGroups.name')
		)
		->where( 'Tags.user_id', $userid )
		->leftJoin('TagGroups','TagGroups.id','=','Tags.tag_Group')
		->process($_POST)
		->json();
