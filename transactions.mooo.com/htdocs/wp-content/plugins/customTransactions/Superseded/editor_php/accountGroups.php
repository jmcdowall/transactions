<?php

// DataTables PHP library
include( "../editor/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
	
$userid = 1;


	Editor::inst( $db, 'AccountGroups' )
		->field( 
			Field::inst( 'name' ),
			Field::inst( 'user_id')
			->setValue( $userid )
		)
		->where( 'AccountGroups.user_id', $userid )
		->process($_POST)
		->json();
