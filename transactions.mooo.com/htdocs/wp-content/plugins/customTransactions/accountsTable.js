
var accountgroups_editor; // use a global for the submit and return data rendering in the examples
var accounts_editor; // use a global for the submit and return data rendering in the examples
var accountgroups_table;
var accounts_table;

$(document).ready(function() {
	
    ///////////////////////////////////////////
	///Logic to Configure Account Groups Table ////
	///////////////////////////////////////////
	accountgroups_editor = new $.fn.dataTable.Editor( {
		ajax:  {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "accountGroups_" + d.action;
        }
    },
		table: "#accountgroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
    accountgroups_table = $('#accountgroups_table').DataTable( {
		dom: "",
		ajax:  {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "accountGroups_" + d.action;
        }
    },
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#accountgroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    accountgroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	accounts_table.ajax.reload();
    //	alert ("Updated");
	} );


	/////////////////////////////////////
	///Logic to Configure Account Table ////
	/////////////////////////////////////
	accounts_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "accounts_" + d.action;
        }
    },
		table: "#accounts_table",
		fields: [ {
				label: "Name:",
				name: "Accounts.Name"
			},
			{
				label: "Description:",
				name: "Accounts.Description",
				type: "text"
			},
			{
				label: "Account Group:",
				name: "Accounts.AccountGroup",
				type: "select"
			},
			{
				label: "Account Type:",
				name: "Accounts.AccountType",
				type: "select"
			},
			{
				label: "Other Grouping:",
				name: "Accounts.OtherGrouping",
				type: "text"
			},
			{
				label: "Include In Cash Total:",
				name: "Accounts.InCash",
				type: "radio",
				options: {
					"No": 0,
        			"Yes":    1
				}
			},
			{
				label: "Include In Net Worth:",
				name: "Accounts.InNetWorth",
				type: "radio",
				options: {
					"No": 0,
        			"Yes":    1
				}
			}
		]
	} );
	
	accounts_table = $('#accounts_table').DataTable( {
		dom: "Brti",
		ajax :  {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "accounts_" + d.action;
        }
    },
		columns: [
			{ data: "Accounts.Name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
			{ data: "Accounts.Description" },
			{ data: "AccountGroups.name", editField: "Accounts.AccountGroup", visible: false },
			{ data: "AccountTypes.name", editField: "Accounts.AccountType" },
			{ data: "Accounts.OtherGrouping" },
			{ data: "Accounts.InCash",  render: function ( data, type, row ) {
        		if(data == 1){
        		return "Y";
        		}
        		else
        		{
        		return "";	
        		}
    } },
			{ data: "Accounts.InNetWorth",  render: function ( data, type, row ) {
        		if(data == 1){
        		return "Y";
        		}
        		else
        		{
        		return "";	
        		} 
			}
			},
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="accounteditor_edit">Edit</a> / <a href="" class="accounteditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="6">'+group+'</td><td><a id="myLink" href="#" onclick="fAccountGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fAccountGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: accounts_editor, text: "New Account" },
			{
                text: "New Account Group",
                action: function ( e, dt, node, config ) {
                  accountgroups_editor.title( 'Add new account group' ).buttons( 'Add' ).create();

                }
            },
            { extend: "edit", editor: accounts_editor, text: "Edit Selected" },
		]

	} );
	
	// Edit record
    accounts_table.on('click', 'a.accounteditor_edit', function (e) {
        e.preventDefault();
 
        accounts_editor.edit( $(this).closest('tr'), {
            title: 'Edit Account',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    accounts_table.on('click', 'a.accounteditor_remove', function (e) {
        e.preventDefault();
 
        accounts_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Account?',
            buttons: 'Delete'
        } );
    } );
} );


function fAccountGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	accountgroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	accountgroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fAccountGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	accountgroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	accountgroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' account group?',
            buttons: 'Delete'
        } );

};

