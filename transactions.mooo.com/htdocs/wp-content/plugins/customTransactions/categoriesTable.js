

var categorygroups_editor; // use a global for the submit and return data rendering in the examples
var categories_editor; // use a global for the submit and return data rendering in the examples
var categorygroups_table;
var categories_table;


$(document).ready(function() {
	

	///////////////////////////////////////////
	///Logic to Configure Category Groups Table ////
	///////////////////////////////////////////
	categorygroups_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "categoryGroups_" + d.action;
        }
    },
		table: "#categorygroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
	categorygroups_table = $('#categorygroups_table').DataTable( {
		dom: "",
		ajax: {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "categoryGroups_" + d.action;
        }
    },
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#categorygroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    categorygroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	categories_table.ajax.reload();
    //	alert ("Updated");
	} );


	/////////////////////////////////////
	///Logic to Configure Categories Table ////
	/////////////////////////////////////
	categories_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "categories_" + d.action;
        }
    },
		table: "#categories_table",
		fields: [ {
				label: "Name:",
				name: "Categories.Name"
			},
			{
				label: "Description:",
				name: "Categories.Description"
			},
			{
				label: "Category Group:",
				name: "Categories.CategoryGroup",
				type: "select",
			},
			{
				label: "Category Type:",
				name: "Categories.Type",
				type: "select"
			},
			{
				label: "Other Grouping:",
				name: "Categories.othergrouping",
				type: "text"
			}
		]
	} );
	
	categories_table = $('#categories_table').DataTable( {
		dom: "Brti",
		ajax: {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "categories_" + d.action;
        }
    },
		columns: [
			{ data: "Categories.Name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
    		{ data: "Categories.Description" },
			{ data: "CategoryGroups.name", editField: "Categories.CategoryGroup", visible: false },
			{ data: "CategoryTypes.name" },
			{ data: "Categories.othergrouping" },
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="categoryeditor_edit">Edit</a> / <a href="" class="categoryeditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="4">'+group+'</td><td><a id="myLink" href="#" onclick="fCategoryGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fCategoryGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: categories_editor, text: "New Category" },
			{
                text: "New Category Group",
                action: function ( e, dt, node, config ) {
                   categorygroups_editor.title( 'Add new category group' ).buttons( 'Add' ).create();

                }
            },
            { extend: "edit", editor: categories_editor, text: "Edit Selected" },
		]

	} );
	
	// Edit record
    categories_table.on('click', 'a.categoryeditor_edit', function (e) {
        e.preventDefault();
 
        categories_editor.edit( $(this).closest('tr'), {
            title: 'Edit Category',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    categories_table.on('click', 'a.categoryeditor_remove', function (e) {
        e.preventDefault();
 
        categories_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Category?',
            buttons: 'Delete'
        } );
    } );
    

} );


function fCategoryGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	categorygroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	categorygroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fCategoryGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	categorygroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	categorygroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' category group?',
            buttons: 'Delete'
        } );

};



