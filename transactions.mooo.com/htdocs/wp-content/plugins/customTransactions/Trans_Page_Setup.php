<?php

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Transactions - Tag Groups</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.1.2/css/keyTable.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/autofill/2.1.2/css/autoFill.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="editor/css/editor.dataTables.min.css">
	<style type="text/css" class="init">
	div.DTE_Inline input {
		border: none;
		background-color: transparent;
		padding: 0 !important;
		font-size: 90%;
	}

	div.DTE_Inline input:focus {
		outline: none;
		background-color: transparent;
	}
	
	dt { margin-top: 1em; }
    dt:first-child { margin-top: 0; }
    dd { width: 25% }
 
    tr.group,
tr.group:hover {
    background-color: #ddd !important;
}

	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/keytable/2.1.2/js/dataTables.keyTable.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/autofill/2.1.2/js/dataTables.autoFill.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="editor/js/dataTables.editor.min.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">
	


var taggroups_editor; // use a global for the submit and return data rendering in the examples
var tags_editor; // use a global for the submit and return data rendering in the examples
var taggroups_table;
var tags_table;

var categorygroups_editor; // use a global for the submit and return data rendering in the examples
var categories_editor; // use a global for the submit and return data rendering in the examples
var categorygroups_table;
var categories_table;

var accountgroups_editor; // use a global for the submit and return data rendering in the examples
var accounts_editor; // use a global for the submit and return data rendering in the examples
var accountgroups_table;
var accounts_table;

$(document).ready(function() {
	
	///////////////////////////////////////////
	///Logic to Configure Tag Groups Table ////
	///////////////////////////////////////////
	taggroups_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/tagGroups.php",
		table: "#taggroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
	taggroups_table = $('#taggroups_table').DataTable( {
		dom: "",
		ajax: "editor_php/tagGroups.php",
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#taggroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    taggroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	tags_table.ajax.reload();
	} );


	/////////////////////////////////////
	///Logic to Configure Tags Table ////
	/////////////////////////////////////
	tags_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/tags.php",
		table: "#tags_table",
		fields: [ {
				label: "Name:",
				name: "Tags.name"
			},
			{
				label: "Tag Group:",
				name: "Tags.tag_Group",
				type: "select"
			}
		]
         
        
	} );
	
	tags_table = $('#tags_table').DataTable( {
		dom: "Brti",
		ajax: "editor_php/tags.php",
		columns: [
			{ data: "Tags.name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
			{ data: "TagGroups.name", editField: "Tags.tag_Group" },
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="tageditor_edit">Edit</a> / <a href="" class="tageditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="2">'+group+'</td><td><a id="myLink" href="#" onclick="fTagGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fTagGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: tags_editor, text: "New Tag" },
			{
                text: "New Group",
                action: function ( e, dt, node, config ) {
                   taggroups_editor.title( 'Add new tag group' ).buttons( 'Add' ).create();

                }
            }
		]

	} );
	
	// Edit record
    tags_table.on('click', 'a.tageditor_edit', function (e) {
        e.preventDefault();
 
        tags_editor.edit( $(this).closest('tr'), {
            title: 'Edit Tag',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    tags_table.on('click', 'a.tageditor_remove', function (e) {
        e.preventDefault();
 
        tags_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Tag?',
            buttons: 'Delete'
        } );
    } );


	///////////////////////////////////////////
	///Logic to Configure Category Groups Table ////
	///////////////////////////////////////////
	categorygroups_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/categoryGroups.php",
		table: "#categorygroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
	categorygroups_table = $('#categorygroups_table').DataTable( {
		dom: "",
		ajax: "editor_php/categoryGroups.php",
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#categorygroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    categorygroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	categories_table.ajax.reload();
    //	alert ("Updated");
	} );


	/////////////////////////////////////
	///Logic to Configure Categories Table ////
	/////////////////////////////////////
	categories_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/categories.php",
		table: "#categories_table",
		fields: [ {
				label: "Name:",
				name: "Categories.Name"
			},
			{
				label: "Category Group:",
				name: "Categories.CategoryGroup",
				type: "select"
			},
			{
				label: "Category Type:",
				name: "Categories.Type",
				type: "select"
			}
		]
	} );
	
	categories_table = $('#categories_table').DataTable( {
		dom: "Brti",
		ajax: "editor_php/categories.php",
		columns: [
			{ data: "Categories.Name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
			{ data: "CategoryGroups.name", editField: "Categories.CategoryGroup" },
			{ data: "CategoryTypes.name" },
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="categoryeditor_edit">Edit</a> / <a href="" class="categoryeditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="3">'+group+'</td><td><a id="myLink" href="#" onclick="fCategoryGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fCategoryGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: categories_editor, text: "New Category" },
			{
                text: "New Category Group",
                action: function ( e, dt, node, config ) {
                   categorygroups_editor.title( 'Add new category group' ).buttons( 'Add' ).create();

                }
            },
            { extend: "edit", editor: categories_editor, text: "Edit Selected" },
		]

	} );
	
	// Edit record
    categories_table.on('click', 'a.categoryeditor_edit', function (e) {
        e.preventDefault();
 
        categories_editor.edit( $(this).closest('tr'), {
            title: 'Edit Category',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    categories_table.on('click', 'a.categoryeditor_remove', function (e) {
        e.preventDefault();
 
        categories_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Category?',
            buttons: 'Delete'
        } );
    } );
    
    ///////////////////////////////////////////
	///Logic to Configure Account Groups Table ////
	///////////////////////////////////////////
	accountgroups_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/accountGroups.php",
		table: "#accountgroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
    accountgroups_table = $('#accountgroups_table').DataTable( {
		dom: "",
		ajax: "editor_php/accountGroups.php",
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#accountgroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    accountgroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	accounts_table.ajax.reload();
    //	alert ("Updated");
	} );


	/////////////////////////////////////
	///Logic to Configure Account Table ////
	/////////////////////////////////////
	accounts_editor = new $.fn.dataTable.Editor( {
		ajax: "editor_php/accounts.php",
		table: "#accounts_table",
		fields: [ {
				label: "Name:",
				name: "Accounts.Name"
			},
			{
				label: "Account Group:",
				name: "Accounts.AccountGroup",
				type: "select"
			},
			{
				label: "Account Type:",
				name: "Accounts.AccountType",
				type: "select"
			}
		]
	} );
	
	accounts_table = $('#accounts_table').DataTable( {
		dom: "Brti",
		ajax: "editor_php/accounts.php",
		columns: [
			{ data: "Accounts.Name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
			{ data: "AccountGroups.name", editField: "Accounts.AccountGroup" },
			{ data: "AccountTypes.name" },
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="accounteditor_edit">Edit</a> / <a href="" class="accounteditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="3">'+group+'</td><td><a id="myLink" href="#" onclick="fAccountGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fAccountGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: accounts_editor, text: "New Account" },
			{
                text: "New Account Group",
                action: function ( e, dt, node, config ) {
                  accountgroups_editor.title( 'Add new account group' ).buttons( 'Add' ).create();

                }
            },
            { extend: "edit", editor: accounts_editor, text: "Edit Selected" },
		]

	} );
	
	// Edit record
    accounts_table.on('click', 'a.accounteditor_edit', function (e) {
        e.preventDefault();
 
        accounts_editor.edit( $(this).closest('tr'), {
            title: 'Edit Account',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    accounts_table.on('click', 'a.accounteditor_remove', function (e) {
        e.preventDefault();
 
        accounts_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Account?',
            buttons: 'Delete'
        } );
    } );
} );


function fTagGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	taggroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	taggroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fTagGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	taggroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	taggroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' tag group?',
            buttons: 'Delete'
        } );

};

function fCategoryGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	categorygroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	categorygroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fCategoryGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	categorygroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	categorygroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' category group?',
            buttons: 'Delete'
        } );

};

function fAccountGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	accountgroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	accountgroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fAccountGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	accountgroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	accountgroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' account group?',
            buttons: 'Delete'
        } );

};




	</script>
</head>
<body class="dt-example">
	<div class="container">
		<section>
			
			<table id="taggroups_table" class="display" cellspacing="0" width="100%">
					<tr>
						<th></th>
						<th>Name</th>
					</tr>
			</table>
			
			<table id="tags_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Tag Group</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>
			
			<BR><BR><BR><BR>
			
			<table id="categorygroups_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
					</tr>

				</thead>

			</table>
			
			<table id="categories_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Category Group</th>
						<th>Category Type</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>
			
			<BR><BR><BR><BR>
			
			<table id="accountgroups_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Name</th>
					</tr>

				</thead>

			</table>
			
			<table id="accounts_table" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Account Group</th>
						<th>Account Type</th>
						<th>Actions</th>
					</tr>

				</thead>

			</table>
			
			
		</section>
	</div>
	<section>
		<div class="footer">
			
		</div>
	</section>
</body>
</html>

<?php

?>