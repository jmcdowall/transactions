
var transactions_editor; // use a global for the submit and return data rendering in the examples
var transactions_table;

$(document).ready(function() {
	
    


	/////////////////////////////////////
	///Logic to Configure Transactions Table ////
	/////////////////////////////////////
	transactions_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url:  ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "transactions_" + d.action;
        }
    },
		table: "#transactions_table",
		fields: [ {
				label: "Date:",
				name: "Transactions.Date",
				type: "datetime",
				def:   function () { return new Date(); }
			},
			{
				label: "Account:",
				name: "Transactions.Account",
				type: "select"
			},
			{ label: "Invoice No:",
				name: "Transactions.InvoiceNo",
				type: "text"},
			{ label: "Payee:",
				name: "Transactions.Payee",
				type: "text"},
			{ label: "Memo:",
				name: "Transactions.Memo",
				type: "text"},
			{ label: "Amount:",
				name: "Transactions.Amount",
				type: "text"},
			{ label: "Category:",
				name: "Transactions.Category",
				type: "select",
				placeholder: "---",
				placeholderDisabled: false,
				placeholderValue: ""},
			{ label: "Tag:",
				name: "Transactions.Tag",
				type: "select",
				placeholder: "---",
				placeholderDisabled: false,
				placeholderValue: ""}
						 
		]
	} );
	
	transations_table = $('#transactions_table').DataTable( {
		dom: "Brti",
		responsive: false,
		ajax :  {
        url:  ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "transactions_" + d.action;
        }
    },
		columns: [
			{data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false},
			{ data: "Transactions.Date"},
			{ data: "Accounts.Name", editField: "Transactions.Account"},
			{ data: "Transactions.InvoiceNo"},
			{ data: "Transactions.Payee"},
			{ data: "Transactions.Memo"},
			{ data: "Transactions.Amount", render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
			{ data: "Categories.Name", editField: "Transactions.Category"},
			{ data: "Tags.Name", editField: "Transactions.Tag"}
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){

 

        },
		buttons: [
			{ extend: "create", editor: transactions_editor, text: "New" },
      { extend: "edit", editor: transactions_editor, text: "Edit" },
			{ extend: "remove", editor: transactions_editor, text: "Remove" }
		]

	} );
	
} );



