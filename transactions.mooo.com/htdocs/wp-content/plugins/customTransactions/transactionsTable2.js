
var transactions_editor; // use a global for the submit and return data rendering in the examples
var transactions_table;

$(document).ready(function() {
	
    


	/////////////////////////////////////
	///Logic to Configure Transactions Table ////
	/////////////////////////////////////
	transactions_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url:  ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "transactions_" + d.action;
        }
    },
		table: "#transactions_table",
		fields: [ {
				label: "Date:",
				name: "Transactions.Date",
				type: "datetime",
				def:   function () { return new Date(); }
			},
			{
				label: "Account:",
				name: "Transactions.Account",
				type: "select",
				placeholder: "---",
				placeholderDisabled: false,
				placeholderValue: ""
			},
			{ label: "Invoice No:",
				name: "Transactions.InvoiceNo",
				type: "text"},
			{ label: "Payee:",
				name: "Transactions.Payee",
				type: "text"},
			{ label: "Memo:",
				name: "Transactions.Memo",
				type: "text"},
			{ label: "Amount:",
				name: "Transactions.Amount",
				type: "text"},
			{ label: "Category:",
				name: "Transactions.Category",
				type: "select",
				placeholder: "---",
				placeholderDisabled: false,
				placeholderValue: ""},
			{ label: "Tag:",
				name: "Transactions.Tag",
				type: "select",
				placeholder: "---",
				placeholderDisabled: false,
				placeholderValue: ""}
						 
		]
	} );
	
		transactions_table = $('#transactions_table').DataTable( {
		dom: "Brti",
		ajax :  {
        url:  ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "transactions_" + d.action;
        }
    },
		columns: [
			{data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false},
			{ data: "Transactions.Date"},
			{ data: "Accounts.Name", editField: "Transactions.Account"},
			{ data: "Transactions.InvoiceNo"},
			{ data: "Transactions.Payee"},
			{ data: "Transactions.Memo"},
			{ data: "Transactions.Amount", render: $.fn.dataTable.render.number( ',', '.', 2, '$' )},
			{ data: "Categories.Name", editField: "Transactions.Category"},
			{ data: "Tags.Name", editField: "Transactions.Tag"},
			{ defaultContent: '', orderable: false, render: $.fn.dataTable.render.number( ',', '.', 2, '$' )}
		],
        keys: {
            columns: [ 1, 2, 3, 4 ,5,6,7,8],
            editor:  transactions_editor,
					focus: ':eq(0)'
        },autoFill: {
            editor:  transactions_editor
        },
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
						var api = this.api();
						var total = 0;
						
	
						//Calculate the total of all the data in the table.
						 api.rows({ order:  'current',  page:   'all',  search: 'applied'}).every(function (rowIdx, tableLoop, rowLoop){
							 total = total + parseFloat(api.cell(rowIdx,6,{ order:  'current',  page:   'all',  search: 'applied'}).data());
        } );
					
					//Loop through all displayed data and generate a running total column
					api.rows({ order:  'current',  page:   'all',  search: 'applied'}).every( function (rowIdx, tableLoop, rowLoop) {
						api.cell(rowIdx,9,{ order:  'current',  page:   'all',  search: 'applied'}).data(total);
						total = total - parseFloat(api.cell(rowIdx,6,{ order:  'current',  page:   'all',  search: 'applied'}).data());
           // cell.innerHTML = api.cell(i,6).data();
        } );
				},
     
		buttons: [
			{ extend: "create", editor: transactions_editor, text: "New" },
      { extend: "edit", editor: transactions_editor, text: "Edit" },
			{ extend: "remove", editor: transactions_editor, text: "Remove" }
		]

	} );
	
	// Disable KeyTable while the main editing form is open
    transactions_editor
        .on( 'open', function ( e, mode, action ) {
            if ( mode === 'main' ) {
                transactions_table.keys.disable();
            }
        } )
        .on( 'close', function () {
            transactions_table.keys.enable();
        } );
	
	// Activate an inline edit on click of a table cell
    $('#transactions_table').on( 'click', 'tbody td:not(:first-child)', function (e) {
    //    transactions_editor.inline( this );
    } );
	
} );



