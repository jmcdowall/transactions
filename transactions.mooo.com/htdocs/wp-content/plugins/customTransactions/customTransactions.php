<?php
/**
 * Plugin Name: Custom Transactions
 * Plugin URI: ...
 * Description: This plugin adds bookeeping support.
 * Version: 1.0.0
 * Author: JM
 * Author URI: 
 * License: N/A
 */
 
include 'ctFunctions.php'; 
// include the Zebra_Form class
include 'zebraForm/Zebra_Form.php';
include 'ctImport.php';
 
add_action('init', 'ct_register_script');

add_action('wp_enqueue_scripts', 'ct_enqueue_scripts');

add_shortcode( 'ct_tagstable', 'ct_shortcode_tagstable' );
 
add_shortcode( 'ct_categoriestable', 'ct_shortcode_categoriestable' );
 
add_shortcode( 'ct_accountstable', 'ct_shortcode_accountstable' );

add_shortcode( 'ct_transactionstable', 'ct_shortcode_transactionstable' );

add_shortcode('ct_transactionsform','ct_shortcode_transactionsform');

add_shortcode('ct_importform','ct_shortcode_importExcel');

//Loops through creating the ajax actions for all possible actions for all tables
$tables = array("accounts","accountGroups","tags","tagGroups","categories","categoryGroups","transactions");
$actions = array("create","delete","edit","upload","","undefined");

foreach($tables as $currentTable){
	foreach($actions as $currentAction){
		add_action( 'wp_ajax_'.$currentTable.'_'.$currentAction, 'ct_ajax_'.$currentTable );
		add_action( 'wp_ajax_nopriv_'.$currentTable.'_'.$currentAction, 'ct_ajax_'.$currentTable );
	}
}



 ?>
 