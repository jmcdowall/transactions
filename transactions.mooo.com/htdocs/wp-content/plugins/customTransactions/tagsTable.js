
var taggroups_editor; // use a global for the submit and return data rendering in the examples
var tags_editor; // use a global for the submit and return data rendering in the examples
var taggroups_table;
var tags_table;


$(document).ready(function() {
	
	///////////////////////////////////////////
	///Logic to Configure Tag Groups Table ////
	///////////////////////////////////////////
	taggroups_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "tagGroups_" + d.action;
        }
    },
		table: "#taggroups_table",
		fields: [ {
				label: "Name:",
				name: "name"
			}
		]
	} );
	
	taggroups_table = $('#taggroups_table').DataTable( {
		dom: "",
		ajax: {
        url: ajax_object.ajax_url,
        "data": function ( d ) {
            d.action = "tagGroups_" + d.action;
        }
    },
		columns: [
			{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
			{ data: "name" }
		],
		keys: {
            columns: ':not(:first-child)',
            keys: [ 9 ]
        },
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
		buttons: [
			
		],
		"initComplete" : function(settings, json) {
			$('#taggroups_table').hide();
		}
	} )
	
	//Update the Tags Table after any modification
    taggroups_editor.on( 'postEdit postCreate postMove', function ( e, json, data ) {
    	tags_table.ajax.reload();
	} );


	/////////////////////////////////////
	///Logic to Configure Tags Table ////
	/////////////////////////////////////
	tags_editor = new $.fn.dataTable.Editor( {
		ajax: {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "tags_" + d.action;
        }
    },
		table: "#tags_table",
		fields: [ {
				label: "Name:",
				name: "Tags.name"
			},
			{
				label: "Tag Group:",
				name: "Tags.tag_Group",
				type: "select"
			}
		]
         
        
	} );
	
	tags_table = $('#tags_table').DataTable( {
		dom: "Brti",
		ajax: {
        url: "../wp-admin/admin-ajax.php",
        "data": function ( d ) {
            d.action = "tags_" + d.action;
        }
    },
		columns: [
			{ data: "Tags.name", render: function ( data, type, full, meta ) {
      return ' - '+data;
    } },
			{ data: "TagGroups.name", editField: "Tags.tag_Group" },
			{
				data: null,
                className: "center",
                defaultContent: '<a href="" class="tageditor_edit">Edit</a> / <a href="" class="tageditor_remove">Delete</a>'
            }
		],
		select: {
            style:    'os',
            selector: 'td',
            blurable: true
        },
        "drawCallback": function(settings){
        	var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="2">'+group+'</td><td><a id="myLink" href="#" onclick="fTagGroup_Edit(\''+group+'\');">Edit</a> / <a id="myLink" href="#" onclick="fTagGroup_Delete(\''+group+'\');">Delete</a></td></tr>'
                    );
 
                    last = group;
               }});

        },
		buttons: [
			{ extend: "create", editor: tags_editor, text: "New Tag" },
			{
                text: "New Group",
                action: function ( e, dt, node, config ) {
                   taggroups_editor.title( 'Add new tag group' ).buttons( 'Add' ).create();

                }
            }
		]

	} );
	
	// Edit record
    tags_table.on('click', 'a.tageditor_edit', function (e) {
        e.preventDefault();
 
        tags_editor.edit( $(this).closest('tr'), {
            title: 'Edit Tag',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    tags_table.on('click', 'a.tageditor_remove', function (e) {
        e.preventDefault();
 
        tags_editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this Tag?',
            buttons: 'Delete'
        } );
    } );

} );


function fTagGroup_Edit(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	taggroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	

	taggroups_editor.edit( RowID, {
            title: 'Edit record',
            buttons: 'Update'
        } );
};

function fTagGroup_Delete(groupname){
	
	var RowID;
	
	//Find the RowID for the selected tag group
	taggroups_table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	    var data = this.data();
	    
	    if( this.data().name === groupname){
	    	RowID = rowIdx;
	    };

	} );
	
	
	taggroups_editor.remove( RowID , {
            title: 'Delete record',
            message: 'Are you sure you wish to remove the '+groupname+ ' tag group?',
            buttons: 'Delete'
        } );

};

