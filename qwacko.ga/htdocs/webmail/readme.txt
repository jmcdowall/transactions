To start automatic installation, run http://your_webmail_web_address/install/ in your web browser.

Installation instructions are available at:
http://www.afterlogic.com/docs/webmail-lite/installation/installation-instructions

If you'd like to upgrade existing product installation rather than installing it from scratch,
check the instructions at:
http://www.afterlogic.com/docs/webmail-lite/installation/upgrading-instructions